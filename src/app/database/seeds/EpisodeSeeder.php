<?php

class EpisodeSeeder extends Seeder
{

    public function run()
    {
        $this->call('EpisodeTableSeeder');

        $this->command->info('Episode table seeded!');
    }

}

class EpisodeTableSeeder extends Seeder
{

    public function run()
    {
        Episode::create(array(
                "episode_number" => "1",
                "show_date"      => "2007-10-26",
                "title"          => "No Agenda 001",
                "link"           => "http://www.noagendanation.com/archive/1",
            )
        );
        Episode::create(array(
                "episode_number" => "2",
                "show_date"      => "2007-11-02",
                "title"          => "No Agenda 002",
                "link"           => "http://www.noagendanation.com/archive/2",
            )
        );
        Episode::create(array(
                "episode_number" => "3",
                "show_date"      => "2007-11-09",
                "title"          => "No Agenda 003",
                "link"           => "http://www.noagendanation.com/archive/3",
            )
        );
        Episode::create(array(
                "episode_number" => "4",
                "show_date"      => "2007-11-17",
                "title"          => "No Agenda 004",
                "link"           => "http://www.noagendanation.com/archive/4",
            )
        );
        Episode::create(array(
                "episode_number" => "5",
                "show_date"      => "2007-11-24",
                "title"          => "No Agenda 005",
                "link"           => "http://www.noagendanation.com/archive/5",
            )
        );
        Episode::create(array(
                "episode_number" => "6",
                "show_date"      => "2007-11-30",
                "title"          => "No Agenda 006",
                "link"           => "http://www.noagendanation.com/archive/6",
            )
        );
        Episode::create(array(
                "episode_number" => "7",
                "show_date"      => "2007-12-07",
                "title"          => "No Agenda 007",
                "link"           => "http://www.noagendanation.com/archive/7",
            )
        );
        Episode::create(array(
                "episode_number" => "8",
                "show_date"      => "2007-12-15",
                "title"          => "No Agenda 008",
                "link"           => "http://www.noagendanation.com/archive/8",
            )
        );
        Episode::create(array(
                "episode_number" => "9",
                "show_date"      => "2007-12-22",
                "title"          => "No Agenda 009",
                "link"           => "http://www.noagendanation.com/archive/9",
            )
        );
        Episode::create(array(
                "episode_number" => "10",
                "show_date"      => "2007-12-29",
                "title"          => "No Agenda 010",
                "link"           => "http://www.noagendanation.com/archive/10",
            )
        );
        Episode::create(array(
                "episode_number" => "11",
                "show_date"      => "2008-01-04",
                "title"          => "8 Pints of Lager please!",
                "link"           => "http://www.noagendanation.com/archive/11",
            )
        );
        Episode::create(array(
                "episode_number" => "12",
                "show_date"      => "2008-01-12",
                "title"          => "Lawsuit Crazy",
                "link"           => "http://www.noagendanation.com/archive/12",
            )
        );
        Episode::create(array(
                "episode_number" => "13",
                "show_date"      => "2008-01-19",
                "title"          => "Turned on by Esther Dyson",
                "link"           => "http://www.noagendanation.com/archive/13",
            )
        );
        Episode::create(array(
                "episode_number" => "14",
                "show_date"      => "2008-01-27",
                "title"          => "Neelis Kroes Is Hot",
                "link"           => "http://www.noagendanation.com/archive/14",
            )
        );
        Episode::create(array(
                "episode_number" => "15",
                "show_date"      => "2008-02-03",
                "title"          => "Hot Horse Meat and Hidden Salami",
                "link"           => "http://www.noagendanation.com/archive/15",
            )
        );
        Episode::create(array(
                "episode_number" => "16",
                "show_date"      => "2008-02-10",
                "title"          => "Start Using Canteens",
                "link"           => "http://www.noagendanation.com/archive/16",
            )
        );
        Episode::create(array(
                "episode_number" => "17",
                "show_date"      => "2008-02-15",
                "title"          => "Gold Toe Socks",
                "link"           => "http://www.noagendanation.com/archive/17",
            )
        );
        Episode::create(array(
                "episode_number" => "18",
                "show_date"      => "2008-02-22",
                "title"          => "Volvo's Are Sexy",
                "link"           => "http://www.noagendanation.com/archive/18",
            )
        );
        Episode::create(array(
                "episode_number" => "19",
                "show_date"      => "2008-03-01",
                "title"          => "Show Me the Money!",
                "link"           => "http://www.noagendanation.com/archive/19",
            )
        );
        Episode::create(array(
                "episode_number" => "20",
                "show_date"      => "2008-03-06",
                "title"          => "Never Say No to a Soldier",
                "link"           => "http://www.noagendanation.com/archive/20",
            )
        );
        Episode::create(array(
                "episode_number" => "21",
                "show_date"      => "2008-03-15",
                "title"          => "Sooty Emissions",
                "link"           => "http://www.noagendanation.com/archive/21",
            )
        );
        Episode::create(array(
                "episode_number" => "23",
                "show_date"      => "2008-03-23",
                "title"          => "Vasectomies and The Fountain of Youth",
                "link"           => "http://www.noagendanation.com/archive/23",
            )
        );
        Episode::create(array(
                "episode_number" => "24",
                "show_date"      => "2008-03-29",
                "title"          => "Bagging Your Own Reality",
                "link"           => "http://www.noagendanation.com/archive/24",
            )
        );
        Episode::create(array(
                "episode_number" => "25",
                "show_date"      => "2008-04-05",
                "title"          => "Grapes the Size of Little Oranges",
                "link"           => "http://www.noagendanation.com/archive/25",
            )
        );
        Episode::create(array(
                "episode_number" => "26",
                "show_date"      => "2008-04-12",
                "title"          => "Feeling Fat?",
                "link"           => "http://www.noagendanation.com/archive/26",
            )
        );
        Episode::create(array(
                "episode_number" => "27",
                "show_date"      => "2008-04-19",
                "title"          => "China Syndrome",
                "link"           => "http://www.noagendanation.com/archive/27",
            )
        );
        Episode::create(array(
                "episode_number" => "28",
                "show_date"      => "2008-04-27",
                "title"          => "The Future of Media",
                "link"           => "http://www.noagendanation.com/archive/28",
            )
        );
        Episode::create(array(
                "episode_number" => "29",
                "show_date"      => "2008-05-03",
                "title"          => "The Zen \"Offer\"",
                "link"           => "http://www.noagendanation.com/archive/29",
            )
        );
        Episode::create(array(
                "episode_number" => "30",
                "show_date"      => "2008-05-10",
                "title"          => "Authoritarianism",
                "link"           => "http://www.noagendanation.com/archive/30",
            )
        );
        Episode::create(array(
                "episode_number" => "31",
                "show_date"      => "2008-05-17",
                "title"          => "Truth to Power Ratio",
                "link"           => "http://www.noagendanation.com/archive/31",
            )
        );
        Episode::create(array(
                "episode_number" => "32",
                "show_date"      => "2008-05-24",
                "title"          => "Benefits Supervisor Sleeping",
                "link"           => "http://www.noagendanation.com/archive/32",
            )
        );
        Episode::create(array(
                "episode_number" => "33",
                "show_date"      => "2008-05-30",
                "title"          => "The King of Beers",
                "link"           => "http://www.noagendanation.com/archive/33",
            )
        );
        Episode::create(array(
                "episode_number" => "34",
                "show_date"      => "2008-06-06",
                "title"          => "200 Dollar Oil",
                "link"           => "http://www.noagendanation.com/archive/34",
            )
        );
        Episode::create(array(
                "episode_number" => "35",
                "show_date"      => "2008-06-14",
                "title"          => "Obama and Olbermann",
                "link"           => "http://www.noagendanation.com/archive/35",
            )
        );
        Episode::create(array(
                "episode_number" => "36",
                "show_date"      => "2008-06-22",
                "title"          => "Yahoo+Microsoft=Britney Spears",
                "link"           => "http://www.noagendanation.com/archive/36",
            )
        );
        Episode::create(array(
                "episode_number" => "37",
                "show_date"      => "2008-06-28",
                "title"          => "Where's The Beef?",
                "link"           => "http://www.noagendanation.com/archive/37",
            )
        );
        Episode::create(array(
                "episode_number" => "38",
                "show_date"      => "2008-07-07",
                "title"          => "Down the Rabbit Hole",
                "link"           => "http://www.noagendanation.com/archive/38",
            )
        );
        Episode::create(array(
                "episode_number" => "39",
                "show_date"      => "2008-07-19",
                "title"          => "Fascism Today",
                "link"           => "http://www.noagendanation.com/archive/39",
            )
        );
        Episode::create(array(
                "episode_number" => "40",
                "show_date"      => "2008-07-27",
                "title"          => "Hydroxy Booster",
                "link"           => "http://www.noagendanation.com/archive/40",
            )
        );
        Episode::create(array(
                "episode_number" => "41",
                "show_date"      => "2008-08-03",
                "title"          => "Planes Trains and Lyndon LaRouche",
                "link"           => "http://www.noagendanation.com/archive/41",
            )
        );
        Episode::create(array(
                "episode_number" => "42",
                "show_date"      => "2008-08-09",
                "title"          => "Paris Hilton Does Zero Point Energy",
                "link"           => "http://www.noagendanation.com/archive/42",
            )
        );
        Episode::create(array(
                "episode_number" => "43",
                "show_date"      => "2008-08-17",
                "title"          => "Almost Live From GitmoNation",
                "link"           => "http://www.noagendanation.com/archive/43",
            )
        );
        Episode::create(array(
                "episode_number" => "44",
                "show_date"      => "2008-08-23",
                "title"          => "Probably a Super Delegate",
                "link"           => "http://www.noagendanation.com/archive/44",
            )
        );
        Episode::create(array(
                "episode_number" => "45",
                "show_date"      => "2008-08-30",
                "title"          => "Do You Think Believe Feel?",
                "link"           => "http://www.noagendanation.com/archive/45",
            )
        );
        Episode::create(array(
                "episode_number" => "46",
                "show_date"      => "2008-09-06",
                "title"          => "Israel to Bomb Iran Nothing to See Here Folks Just Shooting Moose...",
                "link"           => "http://www.noagendanation.com/archive/46",
            )
        );
        Episode::create(array(
                "episode_number" => "47",
                "show_date"      => "2008-09-13",
                "title"          => "Kill Bill",
                "link"           => "http://www.noagendanation.com/archive/47",
            )
        );
        Episode::create(array(
                "episode_number" => "48",
                "show_date"      => "2008-09-21",
                "title"          => "How to Survive the Economic Collapse",
                "link"           => "http://www.noagendanation.com/archive/48",
            )
        );
        Episode::create(array(
                "episode_number" => "49",
                "show_date"      => "2008-09-27",
                "title"          => "Everybody Wants to Rule the World",
                "link"           => "http://www.noagendanation.com/archive/49",
            )
        );
        Episode::create(array(
                "episode_number" => "50",
                "show_date"      => "2008-10-03",
                "title"          => "The Sarah Palin Show",
                "link"           => "http://www.noagendanation.com/archive/50",
            )
        );
        Episode::create(array(
                "episode_number" => "51",
                "show_date"      => "2008-10-12",
                "title"          => "Corked Wine",
                "link"           => "http://www.noagendanation.com/archive/51",
            )
        );
        Episode::create(array(
                "episode_number" => "52",
                "show_date"      => "2008-10-19",
                "title"          => "Solex",
                "link"           => "http://www.noagendanation.com/archive/52",
            )
        );
        Episode::create(array(
                "episode_number" => "53",
                "show_date"      => "2008-10-25",
                "title"          => "Gay Marriage",
                "link"           => "http://www.noagendanation.com/archive/53",
            )
        );
        Episode::create(array(
                "episode_number" => "54",
                "show_date"      => "2008-11-01",
                "title"          => "Obama Armbands",
                "link"           => "http://www.noagendanation.com/archive/54",
            )
        );
        Episode::create(array(
                "episode_number" => "55",
                "show_date"      => "2008-11-05",
                "title"          => "Obama Wins Now What?",
                "link"           => "http://www.noagendanation.com/archive/55",
            )
        );
        Episode::create(array(
                "episode_number" => "56",
                "show_date"      => "2008-11-08",
                "title"          => "100 Billion Dollars!",
                "link"           => "http://www.noagendanation.com/archive/56",
            )
        );
        Episode::create(array(
                "episode_number" => "57",
                "show_date"      => "2008-11-15",
                "title"          => "Michelle Oprah and the Strippers",
                "link"           => "http://www.noagendanation.com/archive/57",
            )
        );
        Episode::create(array(
                "episode_number" => "58",
                "show_date"      => "2008-11-22",
                "title"          => "Cork Grows on Trees",
                "link"           => "http://www.noagendanation.com/archive/58",
            )
        );
        Episode::create(array(
                "episode_number" => "59",
                "show_date"      => "2008-11-28",
                "title"          => "Smells Like Tee Truffle",
                "link"           => "http://www.noagendanation.com/archive/59",
            )
        );
        Episode::create(array(
                "episode_number" => "60",
                "show_date"      => "2008-12-06",
                "title"          => "We're Celebrities Get Us Out of Here!",
                "link"           => "http://www.noagendanation.com/archive/60",
            )
        );
        Episode::create(array(
                "episode_number" => "61",
                "show_date"      => "2008-12-13",
                "title"          => "One Big Ponzi Scheme",
                "link"           => "http://www.noagendanation.com/archive/61",
            )
        );
        Episode::create(array(
                "episode_number" => "62",
                "show_date"      => "2008-12-20",
                "title"          => "The Greatest Depression",
                "link"           => "http://www.noagendanation.com/archive/62",
            )
        );
        Episode::create(array(
                "episode_number" => "63",
                "show_date"      => "2008-12-27",
                "title"          => "Save This Polar Bear",
                "link"           => "http://www.noagendanation.com/archive/63",
            )
        );
        Episode::create(array(
                "episode_number" => "64",
                "show_date"      => "2009-01-03",
                "title"          => "Carbon Credits and the CIA",
                "link"           => "http://www.noagendanation.com/archive/64",
            )
        );
        Episode::create(array(
                "episode_number" => "65",
                "show_date"      => "2009-01-10",
                "title"          => "The Dead Bee Conspiracy",
                "link"           => "http://www.noagendanation.com/archive/65",
            )
        );
        Episode::create(array(
                "episode_number" => "66",
                "show_date"      => "2009-01-17",
                "title"          => "Throwin' The \"Oh\"",
                "link"           => "http://www.noagendanation.com/archive/66",
            )
        );
        Episode::create(array(
                "episode_number" => "67",
                "show_date"      => "2009-01-24",
                "title"          => "Kennedy and the Body Count",
                "link"           => "http://www.noagendanation.com/archive/67",
            )
        );
        Episode::create(array(
                "episode_number" => "68",
                "show_date"      => "2009-02-01",
                "title"          => "Crackpot & The Buzz-Kill",
                "link"           => "http://www.noagendanation.com/archive/68",
            )
        );
        Episode::create(array(
                "episode_number" => "69",
                "show_date"      => "2009-02-05",
                "title"          => "The Third Shoe Show",
                "link"           => "http://www.noagendanation.com/archive/69",
            )
        );
        Episode::create(array(
                "episode_number" => "70",
                "show_date"      => "2009-02-07",
                "title"          => "Short Changed",
                "link"           => "http://www.noagendanation.com/archive/70",
            )
        );
        Episode::create(array(
                "episode_number" => "71",
                "show_date"      => "2009-02-12",
                "title"          => "Be-Wilder-Ment & The Queen",
                "link"           => "http://www.noagendanation.com/archive/71",
            )
        );
        Episode::create(array(
                "episode_number" => "72",
                "show_date"      => "2009-02-15",
                "title"          => "Obama Denver & the New World Order",
                "link"           => "http://www.noagendanation.com/archive/72",
            )
        );
        Episode::create(array(
                "episode_number" => "73",
                "show_date"      => "2009-02-19",
                "title"          => "Save or Create",
                "link"           => "http://www.noagendanation.com/archive/73",
            )
        );
        Episode::create(array(
                "episode_number" => "74",
                "show_date"      => "2009-02-22",
                "title"          => "Enter Colorado",
                "link"           => "http://www.noagendanation.com/archive/74",
            )
        );
        Episode::create(array(
                "episode_number" => "75",
                "show_date"      => "2009-02-26",
                "title"          => "Boeing vs. Airbus - The Flatulence Conspiracy",
                "link"           => "http://www.noagendanation.com/archive/75",
            )
        );
        Episode::create(array(
                "episode_number" => "76",
                "show_date"      => "2009-03-01",
                "title"          => "Iridium Fluoride Marijuana and Pelosi's Puppies",
                "link"           => "http://www.noagendanation.com/archive/76",
            )
        );
        Episode::create(array(
                "episode_number" => "77",
                "show_date"      => "2009-03-05",
                "title"          => "Hot Vegetarian Chicks and Other Deep Thoughts",
                "link"           => "http://www.noagendanation.com/archive/77",
            )
        );
        Episode::create(array(
                "episode_number" => "78",
                "show_date"      => "2009-03-08",
                "title"          => "The Great Daylight Savings Time Conspiracy or 100% Yanni-Free",
                "link"           => "http://www.noagendanation.com/archive/78",
            )
        );
        Episode::create(array(
                "episode_number" => "79",
                "show_date"      => "2009-03-12",
                "title"          => "Something is Amiss in Gitmo and Crackpot Nations",
                "link"           => "http://www.noagendanation.com/archive/79",
            )
        );
        Episode::create(array(
                "episode_number" => "80",
                "show_date"      => "2009-03-15",
                "title"          => "Al Gore: The Most Dangerous Man Alive!!",
                "link"           => "http://www.noagendanation.com/archive/80",
            )
        );
        Episode::create(array(
                "episode_number" => "81",
                "show_date"      => "2009-03-19",
                "title"          => "Naked Vegans in Cages",
                "link"           => "http://www.noagendanation.com/archive/81",
            )
        );
        Episode::create(array(
                "episode_number" => "82",
                "show_date"      => "2009-03-22",
                "title"          => "Boom and Bust Explained",
                "link"           => "http://www.noagendanation.com/archive/82",
            )
        );
        Episode::create(array(
                "episode_number" => "83",
                "show_date"      => "2009-03-25",
                "title"          => "One Too Many Clips",
                "link"           => "http://www.noagendanation.com/archive/83",
            )
        );
        Episode::create(array(
                "episode_number" => "84",
                "show_date"      => "2009-03-29",
                "title"          => "Jobs Justice and Climate",
                "link"           => "http://www.noagendanation.com/archive/84",
            )
        );
        Episode::create(array(
                "episode_number" => "85",
                "show_date"      => "2009-04-02",
                "title"          => "Ketchup is Hard to Make",
                "link"           => "http://www.noagendanation.com/archive/85",
            )
        );
        Episode::create(array(
                "episode_number" => "86",
                "show_date"      => "2009-04-05",
                "title"          => "Fat Chicks from Toronto",
                "link"           => "http://www.noagendanation.com/archive/86",
            )
        );
        Episode::create(array(
                "episode_number" => "87",
                "show_date"      => "2009-04-08",
                "title"          => "Knights of the No Agenda Armory",
                "link"           => "http://www.noagendanation.com/archive/87",
            )
        );
        Episode::create(array(
                "episode_number" => "88",
                "show_date"      => "2009-04-12",
                "title"          => "Perchlorate and Cut Fiber",
                "link"           => "http://www.noagendanation.com/archive/88",
            )
        );
        Episode::create(array(
                "episode_number" => "89",
                "show_date"      => "2009-04-16",
                "title"          => "Nuke the Gay Pirates",
                "link"           => "http://www.noagendanation.com/archive/89",
            )
        );
        Episode::create(array(
                "episode_number" => "90",
                "show_date"      => "2009-04-19",
                "title"          => "Lost Your Job? Eat More Fiber",
                "link"           => "http://www.noagendanation.com/archive/90",
            )
        );
        Episode::create(array(
                "episode_number" => "91",
                "show_date"      => "2009-04-22",
                "title"          => "Stroking the Ugly Stick",
                "link"           => "http://www.noagendanation.com/archive/91",
            )
        );
        Episode::create(array(
                "episode_number" => "92",
                "show_date"      => "2009-04-26",
                "title"          => "Swine Flu: It's a Beta!",
                "link"           => "http://www.noagendanation.com/archive/92",
            )
        );
        Episode::create(array(
                "episode_number" => "93",
                "show_date"      => "2009-04-30",
                "title"          => "Pigs in Space",
                "link"           => "http://www.noagendanation.com/archive/93",
            )
        );
        Episode::create(array(
                "episode_number" => "94",
                "show_date"      => "2009-05-03",
                "title"          => "Dvorak Wears Prada",
                "link"           => "http://www.noagendanation.com/archive/94",
            )
        );
        Episode::create(array(
                "episode_number" => "95",
                "show_date"      => "2009-05-07",
                "title"          => "We're All Terrorists Now",
                "link"           => "http://www.noagendanation.com/archive/95",
            )
        );
        Episode::create(array(
                "episode_number" => "96",
                "show_date"      => "2009-05-10",
                "title"          => "Water Weed And Weasels",
                "link"           => "http://www.noagendanation.com/archive/96",
            )
        );
        Episode::create(array(
                "episode_number" => "97",
                "show_date"      => "2009-05-14",
                "title"          => "Brain Damage",
                "link"           => "http://www.noagendanation.com/archive/97",
            )
        );
        Episode::create(array(
                "episode_number" => "98",
                "show_date"      => "2009-05-17",
                "title"          => "Health Code Violation",
                "link"           => "http://www.noagendanation.com/archive/98",
            )
        );
        Episode::create(array(
                "episode_number" => "99",
                "show_date"      => "2009-05-28",
                "title"          => "Dandelion Wine",
                "link"           => "http://www.noagendanation.com/archive/99",
            )
        );
        Episode::create(array(
                "episode_number" => "100",
                "show_date"      => "2009-05-31",
                "title"          => "A Squirrel Walks Into A Bar",
                "link"           => "http://www.noagendanation.com/archive/100",
            )
        );
        Episode::create(array(
                "episode_number" => "101",
                "show_date"      => "2009-06-03",
                "title"          => "Lightning Strikes",
                "link"           => "http://www.noagendanation.com/archive/101",
            )
        );
        Episode::create(array(
                "episode_number" => "102",
                "show_date"      => "2009-06-07",
                "title"          => "Bizarre Sex Crime",
                "link"           => "http://www.noagendanation.com/archive/102",
            )
        );
        Episode::create(array(
                "episode_number" => "103",
                "show_date"      => "2009-06-11",
                "title"          => "Taylor Swift Sucks",
                "link"           => "http://www.noagendanation.com/archive/103",
            )
        );
        Episode::create(array(
                "episode_number" => "104",
                "show_date"      => "2009-06-14",
                "title"          => "Furries Forever",
                "link"           => "http://www.noagendanation.com/archive/104",
            )
        );
        Episode::create(array(
                "episode_number" => "105",
                "show_date"      => "2009-06-18",
                "title"          => "The French Fry Connection",
                "link"           => "http://www.noagendanation.com/archive/105",
            )
        );
        Episode::create(array(
                "episode_number" => "106",
                "show_date"      => "2009-06-21",
                "title"          => "A Jew In The Safe",
                "link"           => "http://www.noagendanation.com/archive/106",
            )
        );
        Episode::create(array(
                "episode_number" => "107",
                "show_date"      => "2009-06-25",
                "title"          => "Health Insurance Dot Gov",
                "link"           => "http://www.noagendanation.com/archive/107",
            )
        );
        Episode::create(array(
                "episode_number" => "108",
                "show_date"      => "2009-06-28",
                "title"          => "Waxman Is A Dick",
                "link"           => "http://www.noagendanation.com/archive/108",
            )
        );
        Episode::create(array(
                "episode_number" => "109",
                "show_date"      => "2009-07-02",
                "title"          => "Forced Vaccinations",
                "link"           => "http://www.noagendanation.com/archive/109",
            )
        );
        Episode::create(array(
                "episode_number" => "110",
                "show_date"      => "2009-07-06",
                "title"          => "Get A Shot of Protection With The No Agenda Show!",
                "link"           => "http://www.noagendanation.com/archive/110",
            )
        );
        Episode::create(array(
                "episode_number" => "111",
                "show_date"      => "2009-07-09",
                "title"          => "Atlas Shrugged",
                "link"           => "http://www.noagendanation.com/archive/111",
            )
        );
        Episode::create(array(
                "episode_number" => "112",
                "show_date"      => "2009-07-12",
                "title"          => "The Doomsday Box",
                "link"           => "http://www.noagendanation.com/archive/112",
            )
        );
        Episode::create(array(
                "episode_number" => "113",
                "show_date"      => "2009-07-16",
                "title"          => "Surviving The Swine Flu",
                "link"           => "http://www.noagendanation.com/archive/113",
            )
        );
        Episode::create(array(
                "episode_number" => "114",
                "show_date"      => "2009-07-19",
                "title"          => "Obama's Pitch",
                "link"           => "http://www.noagendanation.com/archive/114",
            )
        );
        Episode::create(array(
                "episode_number" => "115",
                "show_date"      => "2009-07-23",
                "title"          => "Explaining The Health Care Bill",
                "link"           => "http://www.noagendanation.com/archive/115",
            )
        );
        Episode::create(array(
                "episode_number" => "116",
                "show_date"      => "2009-07-27",
                "title"          => "German Soldiers On American Soil This Week",
                "link"           => "http://www.noagendanation.com/archive/116",
            )
        );
        Episode::create(array(
                "episode_number" => "117",
                "show_date"      => "2009-07-30",
                "title"          => "No Anthrax For You!",
                "link"           => "http://www.noagendanation.com/archive/117",
            )
        );
        Episode::create(array(
                "episode_number" => "118",
                "show_date"      => "2009-08-02",
                "title"          => "Deconstructing 'Law & Order'",
                "link"           => "http://www.noagendanation.com/archive/118",
            )
        );
        Episode::create(array(
                "episode_number" => "119",
                "show_date"      => "2009-08-06",
                "title"          => "Small Steps Toward Better Health",
                "link"           => "http://www.noagendanation.com/archive/119",
            )
        );
        Episode::create(array(
                "episode_number" => "120",
                "show_date"      => "2009-08-09",
                "title"          => "The Clinton Gore Man Hug",
                "link"           => "http://www.noagendanation.com/archive/120",
            )
        );
        Episode::create(array(
                "episode_number" => "121",
                "show_date"      => "2009-08-13",
                "title"          => "The Vivek Kundra \"Hollow\" Deck",
                "link"           => "http://www.noagendanation.com/archive/121",
            )
        );
        Episode::create(array(
                "episode_number" => "122",
                "show_date"      => "2009-08-16",
                "title"          => "Sebelius Double Speak",
                "link"           => "http://www.noagendanation.com/archive/122",
            )
        );
        Episode::create(array(
                "episode_number" => "123",
                "show_date"      => "2009-08-20",
                "title"          => "Thanksgiving Turkeys",
                "link"           => "http://www.noagendanation.com/archive/123",
            )
        );
        Episode::create(array(
                "episode_number" => "124",
                "show_date"      => "2009-08-23",
                "title"          => "Obama's Lobsters",
                "link"           => "http://www.noagendanation.com/archive/124",
            )
        );
        Episode::create(array(
                "episode_number" => "125",
                "show_date"      => "2009-08-27",
                "title"          => "Breaking News: Ted Kennedy Is Dead",
                "link"           => "http://www.noagendanation.com/archive/125",
            )
        );
        Episode::create(array(
                "episode_number" => "126",
                "show_date"      => "2009-08-30",
                "title"          => "Sell Your Kidney!",
                "link"           => "http://www.noagendanation.com/archive/126",
            )
        );
        Episode::create(array(
                "episode_number" => "127",
                "show_date"      => "2009-09-03",
                "title"          => "Poppy Futures Blooming",
                "link"           => "http://www.noagendanation.com/archive/127",
            )
        );
        Episode::create(array(
                "episode_number" => "128",
                "show_date"      => "2009-09-06",
                "title"          => "Coming Soon: The Cashless Society",
                "link"           => "http://www.noagendanation.com/archive/128",
            )
        );
        Episode::create(array(
                "episode_number" => "129",
                "show_date"      => "2009-09-10",
                "title"          => "Unionize Everything",
                "link"           => "http://www.noagendanation.com/archive/129",
            )
        );
        Episode::create(array(
                "episode_number" => "130",
                "show_date"      => "2009-09-13",
                "title"          => "Cloudbusting",
                "link"           => "http://www.noagendanation.com/archive/130",
            )
        );
        Episode::create(array(
                "episode_number" => "131",
                "show_date"      => "2009-09-17",
                "title"          => "Bill Maher Sucks",
                "link"           => "http://www.noagendanation.com/archive/131",
            )
        );
        Episode::create(array(
                "episode_number" => "132",
                "show_date"      => "2009-09-20",
                "title"          => "Thank You Satan?",
                "link"           => "http://www.noagendanation.com/archive/132",
            )
        );
        Episode::create(array(
                "episode_number" => "133",
                "show_date"      => "2009-09-23",
                "title"          => "The Algae Car Saves Ohio",
                "link"           => "http://www.noagendanation.com/archive/133",
            )
        );
        Episode::create(array(
                "episode_number" => "134",
                "show_date"      => "2009-09-27",
                "title"          => "Fox News Babes",
                "link"           => "http://www.noagendanation.com/archive/134",
            )
        );
        Episode::create(array(
                "episode_number" => "135",
                "show_date"      => "2009-10-01",
                "title"          => "Google Wave Invite",
                "link"           => "http://www.noagendanation.com/archive/135",
            )
        );
        Episode::create(array(
                "episode_number" => "136",
                "show_date"      => "2009-10-04",
                "title"          => "Is iTunes a Government Honeypot?",
                "link"           => "http://www.noagendanation.com/archive/136",
            )
        );
        Episode::create(array(
                "episode_number" => "137",
                "show_date"      => "2009-10-08",
                "title"          => "Miley Cyrus Meets Hannah Montana",
                "link"           => "http://www.noagendanation.com/archive/137",
            )
        );
        Episode::create(array(
                "episode_number" => "138",
                "show_date"      => "2009-10-10",
                "title"          => "Bombing The Moon (Don't look over here!)",
                "link"           => "http://www.noagendanation.com/archive/138",
            )
        );
        Episode::create(array(
                "episode_number" => "139",
                "show_date"      => "2009-10-15",
                "title"          => "Zombieland USA",
                "link"           => "http://www.noagendanation.com/archive/139",
            )
        );
        Episode::create(array(
                "episode_number" => "140",
                "show_date"      => "2009-10-18",
                "title"          => "Pro-Rape Republicans",
                "link"           => "http://www.noagendanation.com/archive/140",
            )
        );
        Episode::create(array(
                "episode_number" => "141",
                "show_date"      => "2009-10-22",
                "title"          => "Who Is John Brek?",
                "link"           => "http://www.noagendanation.com/archive/141",
            )
        );
        Episode::create(array(
                "episode_number" => "142",
                "show_date"      => "2009-10-25",
                "title"          => "Obamaland",
                "link"           => "http://www.noagendanation.com/archive/142",
            )
        );
        Episode::create(array(
                "episode_number" => "143",
                "show_date"      => "2009-10-29",
                "title"          => "Obama Sells Out To China",
                "link"           => "http://www.noagendanation.com/archive/143",
            )
        );
        Episode::create(array(
                "episode_number" => "144",
                "show_date"      => "2009-11-01",
                "title"          => "Hemorrhagic Flu Outbreak!",
                "link"           => "http://www.noagendanation.com/archive/144",
            )
        );
        Episode::create(array(
                "episode_number" => "145",
                "show_date"      => "2009-11-05",
                "title"          => "The WOCU Show",
                "link"           => "http://www.noagendanation.com/archive/145",
            )
        );
        Episode::create(array(
                "episode_number" => "146",
                "show_date"      => "2009-11-08",
                "title"          => "Fort Hood Terrorist",
                "link"           => "http://www.noagendanation.com/archive/146",
            )
        );
        Episode::create(array(
                "episode_number" => "147",
                "show_date"      => "2009-11-11",
                "title"          => "Podcast Award Nominees",
                "link"           => "http://www.noagendanation.com/archive/147",
            )
        );
        Episode::create(array(
                "episode_number" => "148",
                "show_date"      => "2009-11-15",
                "title"          => "Adam Gets Kicked Out",
                "link"           => "http://www.noagendanation.com/archive/148",
            )
        );
        Episode::create(array(
                "episode_number" => "149",
                "show_date"      => "2009-11-19",
                "title"          => "Fools Gold",
                "link"           => "http://www.noagendanation.com/archive/149",
            )
        );
        Episode::create(array(
                "episode_number" => "150",
                "show_date"      => "2009-11-22",
                "title"          => "Global Warming Denialism",
                "link"           => "http://www.noagendanation.com/archive/150",
            )
        );
        Episode::create(array(
                "episode_number" => "151",
                "show_date"      => "2009-11-26",
                "title"          => "Turkeys Going Rogue",
                "link"           => "http://www.noagendanation.com/archive/151",
            )
        );
        Episode::create(array(
                "episode_number" => "152",
                "show_date"      => "2009-11-29",
                "title"          => "Attack Of The Icebergs",
                "link"           => "http://www.noagendanation.com/archive/152",
            )
        );
        Episode::create(array(
                "episode_number" => "153",
                "show_date"      => "2009-12-02",
                "title"          => "350 Protests",
                "link"           => "http://www.noagendanation.com/archive/153",
            )
        );
        Episode::create(array(
                "episode_number" => "154",
                "show_date"      => "2009-12-06",
                "title"          => "Amanda Knox Knockers",
                "link"           => "http://www.noagendanation.com/archive/154",
            )
        );
        Episode::create(array(
                "episode_number" => "155",
                "show_date"      => "2009-12-10",
                "title"          => "Holes Over Norway",
                "link"           => "http://www.noagendanation.com/archive/155",
            )
        );
        Episode::create(array(
                "episode_number" => "156",
                "show_date"      => "2009-12-13",
                "title"          => "The Science is In Part Two",
                "link"           => "http://www.noagendanation.com/archive/156",
            )
        );
        Episode::create(array(
                "episode_number" => "157",
                "show_date"      => "2009-12-16",
                "title"          => "The Democratic Industrial Complex",
                "link"           => "http://www.noagendanation.com/archive/157",
            )
        );
        Episode::create(array(
                "episode_number" => "158",
                "show_date"      => "2009-12-19",
                "title"          => "USA Attacks Yemen",
                "link"           => "http://www.noagendanation.com/archive/158",
            )
        );
        Episode::create(array(
                "episode_number" => "159",
                "show_date"      => "2009-12-24",
                "title"          => "Health Care Doublecross",
                "link"           => "http://www.noagendanation.com/archive/159",
            )
        );
        Episode::create(array(
                "episode_number" => "160",
                "show_date"      => "2009-12-27",
                "title"          => "Yemen and the Nigerian Crotch Bomber",
                "link"           => "http://www.noagendanation.com/archive/160",
            )
        );
        Episode::create(array(
                "episode_number" => "161",
                "show_date"      => "2009-12-31",
                "title"          => "Obama vs CIA",
                "link"           => "http://www.noagendanation.com/archive/161",
            )
        );
        Episode::create(array(
                "episode_number" => "162",
                "show_date"      => "2010-01-03",
                "title"          => "30 Taliban Killed",
                "link"           => "http://www.noagendanation.com/archive/162",
            )
        );
        Episode::create(array(
                "episode_number" => "163",
                "show_date"      => "2010-01-07",
                "title"          => "The Heroin Boot And You",
                "link"           => "http://www.noagendanation.com/archive/163",
            )
        );
        Episode::create(array(
                "episode_number" => "164",
                "show_date"      => "2010-01-10",
                "title"          => "Waterboarding For Everyone!",
                "link"           => "http://www.noagendanation.com/archive/164",
            )
        );
        Episode::create(array(
                "episode_number" => "165",
                "show_date"      => "2010-01-14",
                "title"          => "Earthquake Machine Strikes Haiti",
                "link"           => "http://www.noagendanation.com/archive/165",
            )
        );
        Episode::create(array(
                "episode_number" => "166",
                "show_date"      => "2010-01-17",
                "title"          => "Monica Crowley's Stilettos",
                "link"           => "http://www.noagendanation.com/archive/166",
            )
        );
        Episode::create(array(
                "episode_number" => "167",
                "show_date"      => "2010-01-21",
                "title"          => "Flying Upside Down",
                "link"           => "http://www.noagendanation.com/archive/167",
            )
        );
        Episode::create(array(
                "episode_number" => "168",
                "show_date"      => "2010-01-24",
                "title"          => "Relief From Haiti Relief",
                "link"           => "http://www.noagendanation.com/archive/168",
            )
        );
        Episode::create(array(
                "episode_number" => "169",
                "show_date"      => "2010-01-28",
                "title"          => "Shysters Show Up",
                "link"           => "http://www.noagendanation.com/archive/169",
            )
        );
        Episode::create(array(
                "episode_number" => "170",
                "show_date"      => "2010-01-31",
                "title"          => "Flat Chested Women",
                "link"           => "http://www.noagendanation.com/archive/170",
            )
        );
        Episode::create(array(
                "episode_number" => "171",
                "show_date"      => "2010-02-04",
                "title"          => "Botulism Vaccine Coming",
                "link"           => "http://www.noagendanation.com/archive/171",
            )
        );
        Episode::create(array(
                "episode_number" => "172",
                "show_date"      => "2010-02-07",
                "title"          => "Palin Teapot Party",
                "link"           => "http://www.noagendanation.com/archive/172",
            )
        );
        Episode::create(array(
                "episode_number" => "173",
                "show_date"      => "2010-02-11",
                "title"          => "Zug Haiti Connection?",
                "link"           => "http://www.noagendanation.com/archive/173",
            )
        );
        Episode::create(array(
                "episode_number" => "174",
                "show_date"      => "2010-02-14",
                "title"          => "Gung Ho!! Fat Choy!",
                "link"           => "http://www.noagendanation.com/archive/174",
            )
        );
        Episode::create(array(
                "episode_number" => "175",
                "show_date"      => "2010-02-18",
                "title"          => "Liz Cheney vs. Sarah Palin",
                "link"           => "http://www.noagendanation.com/archive/175",
            )
        );
        Episode::create(array(
                "episode_number" => "176",
                "show_date"      => "2010-02-21",
                "title"          => "Ron Paul For President",
                "link"           => "http://www.noagendanation.com/archive/176",
            )
        );
        Episode::create(array(
                "episode_number" => "177",
                "show_date"      => "2010-02-25",
                "title"          => "Vajazzling the Slutsquad",
                "link"           => "http://www.noagendanation.com/archive/177",
            )
        );
        Episode::create(array(
                "episode_number" => "178",
                "show_date"      => "2010-02-28",
                "title"          => "HAARP-ing on Earthquakes",
                "link"           => "http://www.noagendanation.com/archive/178",
            )
        );
        Episode::create(array(
                "episode_number" => "179",
                "show_date"      => "2010-03-04",
                "title"          => "Douchebags",
                "link"           => "http://www.noagendanation.com/archive/179",
            )
        );
        Episode::create(array(
                "episode_number" => "180",
                "show_date"      => "2010-03-07",
                "title"          => "Media Corruption Now in 3D",
                "link"           => "http://www.noagendanation.com/archive/180",
            )
        );
        Episode::create(array(
                "episode_number" => "181",
                "show_date"      => "2010-03-11",
                "title"          => "Jihad Jane Rides Again",
                "link"           => "http://www.noagendanation.com/archive/181",
            )
        );
        Episode::create(array(
                "episode_number" => "182",
                "show_date"      => "2010-03-14",
                "title"          => "Magnets In Space",
                "link"           => "http://www.noagendanation.com/archive/182",
            )
        );
        Episode::create(array(
                "episode_number" => "183",
                "show_date"      => "2010-03-18",
                "title"          => "Porn In The Morn'",
                "link"           => "http://www.noagendanation.com/archive/183",
            )
        );
        Episode::create(array(
                "episode_number" => "184",
                "show_date"      => "2010-03-21",
                "title"          => "All Your DNA [sic]Belong To Us",
                "link"           => "http://www.noagendanation.com/archive/184",
            )
        );
        Episode::create(array(
                "episode_number" => "185",
                "show_date"      => "2010-03-25",
                "title"          => "VATBS vs AQAP",
                "link"           => "http://www.noagendanation.com/archive/185",
            )
        );
        Episode::create(array(
                "episode_number" => "186",
                "show_date"      => "2010-03-28",
                "title"          => "Just Give Us Your Cash",
                "link"           => "http://www.noagendanation.com/archive/186",
            )
        );
        Episode::create(array(
                "episode_number" => "187",
                "show_date"      => "2010-04-01",
                "title"          => "4-Fools Capsizes",
                "link"           => "http://www.noagendanation.com/archive/187",
            )
        );
        Episode::create(array(
                "episode_number" => "188",
                "show_date"      => "2010-04-04",
                "title"          => "Easter Haiku and iPads For You",
                "link"           => "http://www.noagendanation.com/archive/188",
            )
        );
        Episode::create(array(
                "episode_number" => "189",
                "show_date"      => "2010-04-08",
                "title"          => "Krazy Karzai Kums Klean",
                "link"           => "http://www.noagendanation.com/archive/189",
            )
        );
        Episode::create(array(
                "episode_number" => "190",
                "show_date"      => "2010-04-11",
                "title"          => "Haiti: Genocide By Neglect",
                "link"           => "http://www.noagendanation.com/archive/190",
            )
        );
        Episode::create(array(
                "episode_number" => "191",
                "show_date"      => "2010-04-15",
                "title"          => "Bully for You",
                "link"           => "http://www.noagendanation.com/archive/191",
            )
        );
        Episode::create(array(
                "episode_number" => "192",
                "show_date"      => "2010-04-18",
                "title"          => "Smoke Gets In Your Eyes",
                "link"           => "http://www.noagendanation.com/archive/192",
            )
        );
        Episode::create(array(
                "episode_number" => "193",
                "show_date"      => "2010-04-22",
                "title"          => "Goldman Sachs and the Pedo Bear",
                "link"           => "http://www.noagendanation.com/archive/193",
            )
        );
        Episode::create(array(
                "episode_number" => "194",
                "show_date"      => "2010-04-25",
                "title"          => "Blood Trains",
                "link"           => "http://www.noagendanation.com/archive/194",
            )
        );
        Episode::create(array(
                "episode_number" => "195",
                "show_date"      => "2010-04-29",
                "title"          => "Kidnapping The Truth",
                "link"           => "http://www.noagendanation.com/archive/195",
            )
        );
        Episode::create(array(
                "episode_number" => "196",
                "show_date"      => "2010-05-02",
                "title"          => "Blow The Drill Baby",
                "link"           => "http://www.noagendanation.com/archive/196",
            )
        );
        Episode::create(array(
                "episode_number" => "197",
                "show_date"      => "2010-05-06",
                "title"          => "Salt in the Wound",
                "link"           => "http://www.noagendanation.com/archive/197",
            )
        );
        Episode::create(array(
                "episode_number" => "198",
                "show_date"      => "2010-05-09",
                "title"          => "Things Could Get Complicated",
                "link"           => "http://www.noagendanation.com/archive/198",
            )
        );
        Episode::create(array(
                "episode_number" => "199",
                "show_date"      => "2010-05-13",
                "title"          => "Elephant In The Room",
                "link"           => "http://www.noagendanation.com/archive/199",
            )
        );
        Episode::create(array(
                "episode_number" => "200",
                "show_date"      => "2010-05-16",
                "title"          => "The Deuce!",
                "link"           => "http://www.noagendanation.com/archive/200",
            )
        );
        Episode::create(array(
                "episode_number" => "200.5",
                "show_date"      => "2010-05-17",
                "title"          => "No Agenda 200.5",
                "link"           => "http://www.noagendanation.com/archive/200.5",
            )
        );
        Episode::create(array(
                "episode_number" => "200.6",
                "show_date"      => "2012-07-12",
                "title"          => "Understanding No Agenda",
                "link"           => "http://www.noagendanation.com/archive/200.6",
            )
        );
        Episode::create(array(
                "episode_number" => "201",
                "show_date"      => "2010-05-21",
                "title"          => "The Reluctant Spy",
                "link"           => "http://www.noagendanation.com/archive/201",
            )
        );
        Episode::create(array(
                "episode_number" => "202",
                "show_date"      => "2010-05-23",
                "title"          => "Trains To FEMA",
                "link"           => "http://www.noagendanation.com/archive/202",
            )
        );
        Episode::create(array(
                "episode_number" => "203",
                "show_date"      => "2010-05-27",
                "title"          => "Slaughterhouse Blues",
                "link"           => "http://www.noagendanation.com/archive/203",
            )
        );
        Episode::create(array(
                "episode_number" => "204",
                "show_date"      => "2010-05-30",
                "title"          => "Oil Spill Déjà vu",
                "link"           => "http://www.noagendanation.com/archive/204",
            )
        );
        Episode::create(array(
                "episode_number" => "205",
                "show_date"      => "2010-06-02",
                "title"          => "Two Ugandans One Cup",
                "link"           => "http://www.noagendanation.com/archive/205",
            )
        );
        Episode::create(array(
                "episode_number" => "206",
                "show_date"      => "2010-06-06",
                "title"          => "Cameras as Weapons",
                "link"           => "http://www.noagendanation.com/archive/206",
            )
        );
        Episode::create(array(
                "episode_number" => "207",
                "show_date"      => "2010-06-10",
                "title"          => "What do you call Soy milk?",
                "link"           => "http://www.noagendanation.com/archive/207",
            )
        );
        Episode::create(array(
                "episode_number" => "208",
                "show_date"      => "2010-06-13",
                "title"          => "Fat China",
                "link"           => "http://www.noagendanation.com/archive/208",
            )
        );
        Episode::create(array(
                "episode_number" => "209",
                "show_date"      => "2010-06-17",
                "title"          => "Escrow Schmeshcrow",
                "link"           => "http://www.noagendanation.com/archive/209",
            )
        );
        Episode::create(array(
                "episode_number" => "210",
                "show_date"      => "2010-06-21",
                "title"          => "Medical Marijuana",
                "link"           => "http://www.noagendanation.com/archive/210",
            )
        );
        Episode::create(array(
                "episode_number" => "211",
                "show_date"      => "2010-06-24",
                "title"          => "The Botox Bot",
                "link"           => "http://www.noagendanation.com/archive/211",
            )
        );
        Episode::create(array(
                "episode_number" => "212",
                "show_date"      => "2010-06-27",
                "title"          => "Billion Dollar Riot",
                "link"           => "http://www.noagendanation.com/archive/212",
            )
        );
        Episode::create(array(
                "episode_number" => "213",
                "show_date"      => "2010-07-01",
                "title"          => "Post Traumatic Sex",
                "link"           => "http://www.noagendanation.com/archive/213",
            )
        );
        Episode::create(array(
                "episode_number" => "214",
                "show_date"      => "2010-07-04",
                "title"          => "MAFIFA",
                "link"           => "http://www.noagendanation.com/archive/214",
            )
        );
        Episode::create(array(
                "episode_number" => "215",
                "show_date"      => "2010-07-08",
                "title"          => "Drunk in America",
                "link"           => "http://www.noagendanation.com/archive/215",
            )
        );
        Episode::create(array(
                "episode_number" => "216",
                "show_date"      => "2010-07-11",
                "title"          => "Doomsday is Tomorrow",
                "link"           => "http://www.noagendanation.com/archive/216",
            )
        );
        Episode::create(array(
                "episode_number" => "217",
                "show_date"      => "2010-07-15",
                "title"          => "The Digital Drug Menace",
                "link"           => "http://www.noagendanation.com/archive/217",
            )
        );
        Episode::create(array(
                "episode_number" => "218",
                "show_date"      => "2010-07-18",
                "title"          => "Former Soviet Spy",
                "link"           => "http://www.noagendanation.com/archive/218",
            )
        );
        Episode::create(array(
                "episode_number" => "219",
                "show_date"      => "2010-07-21",
                "title"          => "Train Crashes into Plane",
                "link"           => "http://www.noagendanation.com/archive/219",
            )
        );
        Episode::create(array(
                "episode_number" => "220",
                "show_date"      => "2010-07-25",
                "title"          => "Hillary For President!",
                "link"           => "http://www.noagendanation.com/archive/220",
            )
        );
        Episode::create(array(
                "episode_number" => "221",
                "show_date"      => "2010-07-29",
                "title"          => "Marceaux for Governor",
                "link"           => "http://www.noagendanation.com/archive/221",
            )
        );
        Episode::create(array(
                "episode_number" => "222",
                "show_date"      => "2010-07-31",
                "title"          => "Earle in the Gulf",
                "link"           => "http://www.noagendanation.com/archive/222",
            )
        );
        Episode::create(array(
                "episode_number" => "223",
                "show_date"      => "2010-08-05",
                "title"          => "Chillies in the Antilles",
                "link"           => "http://www.noagendanation.com/archive/223",
            )
        );
        Episode::create(array(
                "episode_number" => "224",
                "show_date"      => "2010-08-08",
                "title"          => "Let Them Eat Hot Pockets",
                "link"           => "http://www.noagendanation.com/archive/224",
            )
        );
        Episode::create(array(
                "episode_number" => "225",
                "show_date"      => "2010-08-12",
                "title"          => "Checkpoint Nation",
                "link"           => "http://www.noagendanation.com/archive/225",
            )
        );
        Episode::create(array(
                "episode_number" => "226",
                "show_date"      => "2010-08-15",
                "title"          => "Your Ankle Bracelet Awaits",
                "link"           => "http://www.noagendanation.com/archive/226",
            )
        );
        Episode::create(array(
                "episode_number" => "227",
                "show_date"      => "2010-08-19",
                "title"          => "Eat, Pray, Love. In The Morning",
                "link"           => "http://www.noagendanation.com/archive/227",
            )
        );
        Episode::create(array(
                "episode_number" => "228",
                "show_date"      => "2010-08-22",
                "title"          => "Gassed in Boston",
                "link"           => "http://www.noagendanation.com/archive/228",
            )
        );
        Episode::create(array(
                "episode_number" => "229",
                "show_date"      => "2010-08-26",
                "title"          => "Eggs & Poop",
                "link"           => "http://www.noagendanation.com/archive/229",
            )
        );
        Episode::create(array(
                "episode_number" => "230",
                "show_date"      => "2010-08-29",
                "title"          => "Battle of the Billionaires",
                "link"           => "http://www.noagendanation.com/archive/230",
            )
        );
        Episode::create(array(
                "episode_number" => "231",
                "show_date"      => "2010-09-02",
                "title"          => "Who's Retarded Now?",
                "link"           => "http://www.noagendanation.com/archive/231",
            )
        );
        Episode::create(array(
                "episode_number" => "232",
                "show_date"      => "2010-09-05",
                "title"          => "Eating the Evidence",
                "link"           => "http://www.noagendanation.com/archive/232",
            )
        );
        Episode::create(array(
                "episode_number" => "233",
                "show_date"      => "2010-09-09",
                "title"          => "Jean versus Sean",
                "link"           => "http://www.noagendanation.com/archive/233",
            )
        );
        Episode::create(array(
                "episode_number" => "234",
                "show_date"      => "2010-09-12",
                "title"          => "Phasers on Stun",
                "link"           => "http://www.noagendanation.com/archive/234",
            )
        );
        Episode::create(array(
                "episode_number" => "235",
                "show_date"      => "2010-09-16",
                "title"          => "Disaster Capitalist",
                "link"           => "http://www.noagendanation.com/archive/235",
            )
        );
        Episode::create(array(
                "episode_number" => "236",
                "show_date"      => "2010-09-19",
                "title"          => "Kids with Depression",
                "link"           => "http://www.noagendanation.com/archive/236",
            )
        );
        Episode::create(array(
                "episode_number" => "237",
                "show_date"      => "2010-09-23",
                "title"          => "Just Hiking",
                "link"           => "http://www.noagendanation.com/archive/237",
            )
        );
        Episode::create(array(
                "episode_number" => "238",
                "show_date"      => "2010-09-26",
                "title"          => "Hawaii-Five-Blows",
                "link"           => "http://www.noagendanation.com/archive/238",
            )
        );
        Episode::create(array(
                "episode_number" => "239",
                "show_date"      => "2010-09-30",
                "title"          => "Change Comes From GNU",
                "link"           => "http://www.noagendanation.com/archive/239",
            )
        );
        Episode::create(array(
                "episode_number" => "240",
                "show_date"      => "2010-10-03",
                "title"          => "Prince Charles is Gay",
                "link"           => "http://www.noagendanation.com/archive/240",
            )
        );
        Episode::create(array(
                "episode_number" => "241",
                "show_date"      => "2010-10-07",
                "title"          => "Germany Boy",
                "link"           => "http://www.noagendanation.com/archive/241",
            )
        );
        Episode::create(array(
                "episode_number" => "242",
                "show_date"      => "2010-10-10",
                "title"          => "Squalid Nullification",
                "link"           => "http://www.noagendanation.com/archive/242",
            )
        );
        Episode::create(array(
                "episode_number" => "243",
                "show_date"      => "2010-10-14",
                "title"          => "Big Soda",
                "link"           => "http://www.noagendanation.com/archive/243",
            )
        );
        Episode::create(array(
                "episode_number" => "244",
                "show_date"      => "2010-10-17",
                "title"          => "Form 990",
                "link"           => "http://www.noagendanation.com/archive/244",
            )
        );
        Episode::create(array(
                "episode_number" => "245",
                "show_date"      => "2010-10-21",
                "title"          => "Bow to the Aqua Buddha",
                "link"           => "http://www.noagendanation.com/archive/245",
            )
        );
        Episode::create(array(
                "episode_number" => "246",
                "show_date"      => "2010-10-24",
                "title"          => "Code for Biodiversity!",
                "link"           => "http://www.noagendanation.com/archive/246",
            )
        );
        Episode::create(array(
                "episode_number" => "247",
                "show_date"      => "2010-10-28",
                "title"          => "Obama Insane?",
                "link"           => "http://www.noagendanation.com/archive/247",
            )
        );
        Episode::create(array(
                "episode_number" => "248",
                "show_date"      => "2010-10-31",
                "title"          => "Stop Albedo Now!",
                "link"           => "http://www.noagendanation.com/archive/248",
            )
        );
        Episode::create(array(
                "episode_number" => "249",
                "show_date"      => "2010-11-04",
                "title"          => "Multidimensional Poverty Index",
                "link"           => "http://www.noagendanation.com/archive/249",
            )
        );
        Episode::create(array(
                "episode_number" => "250",
                "show_date"      => "2010-11-04",
                "title"          => "Transportation Sexual Assault (TSA)",
                "link"           => "http://www.noagendanation.com/archive/250",
            )
        );
        Episode::create(array(
                "episode_number" => "251",
                "show_date"      => "2010-11-11",
                "title"          => "Banned by NASA",
                "link"           => "http://www.noagendanation.com/archive/251",
            )
        );
        Episode::create(array(
                "episode_number" => "252",
                "show_date"      => "2010-11-14",
                "title"          => "Debriefing Flameless Fire",
                "link"           => "http://www.noagendanation.com/archive/252",
            )
        );
        Episode::create(array(
                "episode_number" => "253",
                "show_date"      => "2010-11-18",
                "title"          => "Love Pats",
                "link"           => "http://www.noagendanation.com/archive/253",
            )
        );
        Episode::create(array(
                "episode_number" => "254",
                "show_date"      => "2010-11-21",
                "title"          => "Final Jeopardy",
                "link"           => "http://www.noagendanation.com/archive/254",
            )
        );
        Episode::create(array(
                "episode_number" => "255",
                "show_date"      => "2010-11-25",
                "title"          => "Radioactive Butts",
                "link"           => "http://www.noagendanation.com/archive/255",
            )
        );
        Episode::create(array(
                "episode_number" => "256",
                "show_date"      => "2010-11-28",
                "title"          => "Cheerleaders for Science",
                "link"           => "http://www.noagendanation.com/archive/256",
            )
        );
        Episode::create(array(
                "episode_number" => "257",
                "show_date"      => "2010-12-02",
                "title"          => "The Moment of Truth",
                "link"           => "http://www.noagendanation.com/archive/257",
            )
        );
        Episode::create(array(
                "episode_number" => "258",
                "show_date"      => "2010-12-05",
                "title"          => "Persistent Jet Contrails",
                "link"           => "http://www.noagendanation.com/archive/258",
            )
        );
        Episode::create(array(
                "episode_number" => "259",
                "show_date"      => "2010-12-09",
                "title"          => "Lucy Napolitano in Yemen",
                "link"           => "http://www.noagendanation.com/archive/259",
            )
        );
        Episode::create(array(
                "episode_number" => "260",
                "show_date"      => "2010-12-12",
                "title"          => "Madoff has left the building",
                "link"           => "http://www.noagendanation.com/archive/260",
            )
        );
        Episode::create(array(
                "episode_number" => "261",
                "show_date"      => "2010-12-16",
                "title"          => "AQ-USA",
                "link"           => "http://www.noagendanation.com/archive/261",
            )
        );
        Episode::create(array(
                "episode_number" => "262",
                "show_date"      => "2010-12-19",
                "title"          => "Mothra Will Save Us!",
                "link"           => "http://www.noagendanation.com/archive/262",
            )
        );
        Episode::create(array(
                "episode_number" => "263",
                "show_date"      => "2010-12-23",
                "title"          => "Don't Ask, Don't Yell",
                "link"           => "http://www.noagendanation.com/archive/263",
            )
        );
        Episode::create(array(
                "episode_number" => "264",
                "show_date"      => "2010-12-26",
                "title"          => "Chatter on the Interwebs",
                "link"           => "http://www.noagendanation.com/archive/264",
            )
        );
        Episode::create(array(
                "episode_number" => "265",
                "show_date"      => "2010-12-30",
                "title"          => "Moon Base on Mars",
                "link"           => "http://www.noagendanation.com/archive/265",
            )
        );
        Episode::create(array(
                "episode_number" => "266",
                "show_date"      => "2011-01-02",
                "title"          => "Dead Angry Birds",
                "link"           => "http://www.noagendanation.com/archive/266",
            )
        );
        Episode::create(array(
                "episode_number" => "267",
                "show_date"      => "2011-01-06",
                "title"          => "What's My Line?",
                "link"           => "http://www.noagendanation.com/archive/267",
            )
        );
        Episode::create(array(
                "episode_number" => "268",
                "show_date"      => "2011-01-09",
                "title"          => "Mavericks of Media",
                "link"           => "http://www.noagendanation.com/archive/268",
            )
        );
        Episode::create(array(
                "episode_number" => "269",
                "show_date"      => "2011-01-13",
                "title"          => "Time Warner Sucks",
                "link"           => "http://www.noagendanation.com/archive/269",
            )
        );
        Episode::create(array(
                "episode_number" => "270",
                "show_date"      => "2011-01-16",
                "title"          => "Call of Doody",
                "link"           => "http://www.noagendanation.com/archive/270",
            )
        );
        Episode::create(array(
                "episode_number" => "271",
                "show_date"      => "2011-01-20",
                "title"          => "By Executive Order",
                "link"           => "http://www.noagendanation.com/archive/271",
            )
        );
        Episode::create(array(
                "episode_number" => "272",
                "show_date"      => "2011-01-23",
                "title"          => "Brown is the New Green",
                "link"           => "http://www.noagendanation.com/archive/272",
            )
        );
        Episode::create(array(
                "episode_number" => "273",
                "show_date"      => "2011-01-27",
                "title"          => "Nap for Humanity",
                "link"           => "http://www.noagendanation.com/archive/273",
            )
        );
        Episode::create(array(
                "episode_number" => "274",
                "show_date"      => "2011-01-30",
                "title"          => "GaGa vs Bieber",
                "link"           => "http://www.noagendanation.com/archive/274",
            )
        );
        Episode::create(array(
                "episode_number" => "275",
                "show_date"      => "2011-02-03",
                "title"          => "The New Normal",
                "link"           => "http://www.noagendanation.com/archive/275",
            )
        );
        Episode::create(array(
                "episode_number" => "276",
                "show_date"      => "2011-02-06",
                "title"          => "Cleopatra Returns",
                "link"           => "http://www.noagendanation.com/archive/276",
            )
        );
        Episode::create(array(
                "episode_number" => "277",
                "show_date"      => "2011-02-10",
                "title"          => "Gingerbread Nation",
                "link"           => "http://www.noagendanation.com/archive/277",
            )
        );
        Episode::create(array(
                "episode_number" => "278",
                "show_date"      => "2011-02-13",
                "title"          => "Pap Schmear",
                "link"           => "http://www.noagendanation.com/archive/278",
            )
        );
        Episode::create(array(
                "episode_number" => "279",
                "show_date"      => "2011-02-17",
                "title"          => "Dr. Watson I presume?",
                "link"           => "http://www.noagendanation.com/archive/279",
            )
        );
        Episode::create(array(
                "episode_number" => "280",
                "show_date"      => "2011-02-20",
                "title"          => "Husslin' for Humanity",
                "link"           => "http://www.noagendanation.com/archive/280",
            )
        );
        Episode::create(array(
                "episode_number" => "281",
                "show_date"      => "2011-02-24",
                "title"          => "Escape from Madrid",
                "link"           => "http://www.noagendanation.com/archive/281",
            )
        );
        Episode::create(array(
                "episode_number" => "282",
                "show_date"      => "2011-02-27",
                "title"          => "GaGa & Little Boy",
                "link"           => "http://www.noagendanation.com/archive/282",
            )
        );
        Episode::create(array(
                "episode_number" => "283",
                "show_date"      => "2011-03-03",
                "title"          => "Farsi Farce",
                "link"           => "http://www.noagendanation.com/archive/283",
            )
        );
        Episode::create(array(
                "episode_number" => "284",
                "show_date"      => "2011-03-06",
                "title"          => "Study says... Duh!",
                "link"           => "http://www.noagendanation.com/archive/284",
            )
        );
        Episode::create(array(
                "episode_number" => "285",
                "show_date"      => "2011-03-10",
                "title"          => "Terror Aperture",
                "link"           => "http://www.noagendanation.com/archive/285",
            )
        );
        Episode::create(array(
                "episode_number" => "286",
                "show_date"      => "2011-03-13",
                "title"          => "Wonton Violence",
                "link"           => "http://www.noagendanation.com/archive/286",
            )
        );
        Episode::create(array(
                "episode_number" => "287",
                "show_date"      => "2011-03-17",
                "title"          => "Death or Worse",
                "link"           => "http://www.noagendanation.com/archive/287",
            )
        );
        Episode::create(array(
                "episode_number" => "288",
                "show_date"      => "2011-03-20",
                "title"          => "George W. Obama",
                "link"           => "http://www.noagendanation.com/archive/288",
            )
        );
        Episode::create(array(
                "episode_number" => "289",
                "show_date"      => "2011-03-24",
                "title"          => "Chillin' & Killin'",
                "link"           => "http://www.noagendanation.com/archive/289",
            )
        );
        Episode::create(array(
                "episode_number" => "290",
                "show_date"      => "2011-03-27",
                "title"          => "Mission Accomplished!",
                "link"           => "http://www.noagendanation.com/archive/290",
            )
        );
        Episode::create(array(
                "episode_number" => "291",
                "show_date"      => "2011-03-31",
                "title"          => "Unconstitutional Botox",
                "link"           => "http://www.noagendanation.com/archive/291",
            )
        );
        Episode::create(array(
                "episode_number" => "292",
                "show_date"      => "2011-04-03",
                "title"          => "Obama Needs Water",
                "link"           => "http://www.noagendanation.com/archive/292",
            )
        );
        Episode::create(array(
                "episode_number" => "293",
                "show_date"      => "2011-04-07",
                "title"          => "Self Radicalize!",
                "link"           => "http://www.noagendanation.com/archive/293",
            )
        );
        Episode::create(array(
                "episode_number" => "294",
                "show_date"      => "2011-04-10",
                "title"          => "Choking the Puffin",
                "link"           => "http://www.noagendanation.com/archive/294",
            )
        );
        Episode::create(array(
                "episode_number" => "295",
                "show_date"      => "2011-04-14",
                "title"          => "Poledancing for College",
                "link"           => "http://www.noagendanation.com/archive/295",
            )
        );
        Episode::create(array(
                "episode_number" => "296",
                "show_date"      => "2011-04-17",
                "title"          => "Highway to Hubris",
                "link"           => "http://www.noagendanation.com/archive/296",
            )
        );
        Episode::create(array(
                "episode_number" => "297",
                "show_date"      => "2011-04-21",
                "title"          => "Obama's Clutch Car",
                "link"           => "http://www.noagendanation.com/archive/297",
            )
        );
        Episode::create(array(
                "episode_number" => "298",
                "show_date"      => "2011-04-24",
                "title"          => "Hot Mature Plumpers",
                "link"           => "http://www.noagendanation.com/archive/298",
            )
        );
        Episode::create(array(
                "episode_number" => "299",
                "show_date"      => "2011-04-28",
                "title"          => "It's Tiara Time!",
                "link"           => "http://www.noagendanation.com/archive/299",
            )
        );
        Episode::create(array(
                "episode_number" => "300",
                "show_date"      => "2011-05-01",
                "title"          => "Show 300!",
                "link"           => "http://www.noagendanation.com/archive/300",
            )
        );
        Episode::create(array(
                "episode_number" => "301",
                "show_date"      => "2011-05-05",
                "title"          => "Code 33",
                "link"           => "http://www.noagendanation.com/archive/301",
            )
        );
        Episode::create(array(
                "episode_number" => "302",
                "show_date"      => "2011-05-08",
                "title"          => "Web Savvy Wolf",
                "link"           => "http://www.noagendanation.com/archive/302",
            )
        );
        Episode::create(array(
                "episode_number" => "303",
                "show_date"      => "2011-05-12",
                "title"          => "Starship Troopers 4",
                "link"           => "http://www.noagendanation.com/archive/303",
            )
        );
        Episode::create(array(
                "episode_number" => "304",
                "show_date"      => "2011-05-15",
                "title"          => "Chimp in Heat",
                "link"           => "http://www.noagendanation.com/archive/304",
            )
        );
        Episode::create(array(
                "episode_number" => "305",
                "show_date"      => "2011-05-19",
                "title"          => "Last Show Ever",
                "link"           => "http://www.noagendanation.com/archive/305",
            )
        );
        Episode::create(array(
                "episode_number" => "306",
                "show_date"      => "2011-05-22",
                "title"          => "We Live!!!",
                "link"           => "http://www.noagendanation.com/archive/306",
            )
        );
        Episode::create(array(
                "episode_number" => "307",
                "show_date"      => "2011-05-26",
                "title"          => "US-EU Framework Agreement",
                "link"           => "http://www.noagendanation.com/archive/307",
            )
        );
        Episode::create(array(
                "episode_number" => "308",
                "show_date"      => "2011-05-29",
                "title"          => "Wiener-Gate",
                "link"           => "http://www.noagendanation.com/archive/308",
            )
        );
        Episode::create(array(
                "episode_number" => "309",
                "show_date"      => "2011-06-02",
                "title"          => "Syria Be Next",
                "link"           => "http://www.noagendanation.com/archive/309",
            )
        );
        Episode::create(array(
                "episode_number" => "310",
                "show_date"      => "2011-06-05",
                "title"          => "Hail the Foot",
                "link"           => "http://www.noagendanation.com/archive/310",
            )
        );
        Episode::create(array(
                "episode_number" => "311",
                "show_date"      => "2011-06-09",
                "title"          => "Holy e-Coli Batman",
                "link"           => "http://www.noagendanation.com/archive/311",
            )
        );
        Episode::create(array(
                "episode_number" => "312",
                "show_date"      => "2011-06-12",
                "title"          => "Illegal in Tennessee",
                "link"           => "http://www.noagendanation.com/archive/312",
            )
        );
        Episode::create(array(
                "episode_number" => "313",
                "show_date"      => "2011-06-16",
                "title"          => "Hide Your Forks",
                "link"           => "http://www.noagendanation.com/archive/313",
            )
        );
        Episode::create(array(
                "episode_number" => "314",
                "show_date"      => "2011-06-19",
                "title"          => "Qaeda CEO",
                "link"           => "http://www.noagendanation.com/archive/314",
            )
        );
        Episode::create(array(
                "episode_number" => "315",
                "show_date"      => "2011-06-23",
                "title"          => "Agenda 21",
                "link"           => "http://www.noagendanation.com/archive/315",
            )
        );
        Episode::create(array(
                "episode_number" => "316",
                "show_date"      => "2011-06-26",
                "title"          => "Adios, MOFO!",
                "link"           => "http://www.noagendanation.com/archive/316",
            )
        );
        Episode::create(array(
                "episode_number" => "317",
                "show_date"      => "2011-06-30",
                "title"          => "Blood and Treasure",
                "link"           => "http://www.noagendanation.com/archive/317",
            )
        );
        Episode::create(array(
                "episode_number" => "318",
                "show_date"      => "2011-07-03",
                "title"          => "Reckless & Provocative",
                "link"           => "http://www.noagendanation.com/archive/318",
            )
        );
        Episode::create(array(
                "episode_number" => "319",
                "show_date"      => "2011-07-07",
                "title"          => "Boob Bombs!",
                "link"           => "http://www.noagendanation.com/archive/319",
            )
        );
        Episode::create(array(
                "episode_number" => "320",
                "show_date"      => "2011-07-10",
                "title"          => "Dead Man Walking",
                "link"           => "http://www.noagendanation.com/archive/320",
            )
        );
        Episode::create(array(
                "episode_number" => "321",
                "show_date"      => "2011-07-14",
                "title"          => "Internet in a Suitcase",
                "link"           => "http://www.noagendanation.com/archive/321",
            )
        );
        Episode::create(array(
                "episode_number" => "322",
                "show_date"      => "2011-07-17",
                "title"          => "Pastafarians Unite!",
                "link"           => "http://www.noagendanation.com/archive/322",
            )
        );
        Episode::create(array(
                "episode_number" => "323",
                "show_date"      => "2011-07-21",
                "title"          => "CIA vs MI6",
                "link"           => "http://www.noagendanation.com/archive/323",
            )
        );
        Episode::create(array(
                "episode_number" => "324",
                "show_date"      => "2011-07-24",
                "title"          => "Cut, Cap, Duck & Cover",
                "link"           => "http://www.noagendanation.com/archive/324",
            )
        );
        Episode::create(array(
                "episode_number" => "325",
                "show_date"      => "2011-07-28",
                "title"          => "Axis of Abuse",
                "link"           => "http://www.noagendanation.com/archive/325",
            )
        );
        Episode::create(array(
                "episode_number" => "326",
                "show_date"      => "2011-07-31",
                "title"          => "Carbon Cops",
                "link"           => "http://www.noagendanation.com/archive/326",
            )
        );
        Episode::create(array(
                "episode_number" => "327",
                "show_date"      => "2011-08-04",
                "title"          => "Aromatic Poo",
                "link"           => "http://www.noagendanation.com/archive/327",
            )
        );
        Episode::create(array(
                "episode_number" => "328",
                "show_date"      => "2011-08-07",
                "title"          => "Pounding the Pavement",
                "link"           => "http://www.noagendanation.com/archive/328",
            )
        );
        Episode::create(array(
                "episode_number" => "329",
                "show_date"      => "2011-08-11",
                "title"          => "Two Batteries One Cup",
                "link"           => "http://www.noagendanation.com/archive/329",
            )
        );
        Episode::create(array(
                "episode_number" => "330",
                "show_date"      => "2011-08-14",
                "title"          => "You Can Take That To The iBank!",
                "link"           => "http://www.noagendanation.com/archive/330",
            )
        );
        Episode::create(array(
                "episode_number" => "331",
                "show_date"      => "2011-08-18",
                "title"          => "Ricin Beans",
                "link"           => "http://www.noagendanation.com/archive/331",
            )
        );
        Episode::create(array(
                "episode_number" => "332",
                "show_date"      => "2011-08-21",
                "title"          => "Bud Nip",
                "link"           => "http://www.noagendanation.com/archive/332",
            )
        );
        Episode::create(array(
                "episode_number" => "333",
                "show_date"      => "2011-08-25",
                "title"          => "Lions Stood Still",
                "link"           => "http://www.noagendanation.com/archive/333",
            )
        );
        Episode::create(array(
                "episode_number" => "334",
                "show_date"      => "2011-08-28",
                "title"          => "\"Hunker Down\"",
                "link"           => "http://www.noagendanation.com/archive/334",
            )
        );
        Episode::create(array(
                "episode_number" => "335",
                "show_date"      => "2011-09-01",
                "title"          => "Aardvark Effect",
                "link"           => "http://www.noagendanation.com/archive/335",
            )
        );
        Episode::create(array(
                "episode_number" => "336",
                "show_date"      => "2011-09-04",
                "title"          => "Tesla Dome",
                "link"           => "http://www.noagendanation.com/archive/336",
            )
        );
        Episode::create(array(
                "episode_number" => "337",
                "show_date"      => "2011-09-08",
                "title"          => "Constitutional Values",
                "link"           => "http://www.noagendanation.com/archive/337",
            )
        );
        Episode::create(array(
                "episode_number" => "338",
                "show_date"      => "2011-09-11",
                "title"          => "Bunga Bunga",
                "link"           => "http://www.noagendanation.com/archive/338",
            )
        );
        Episode::create(array(
                "episode_number" => "339",
                "show_date"      => "2011-09-15",
                "title"          => "Cocked Pistol",
                "link"           => "http://www.noagendanation.com/archive/339",
            )
        );
        Episode::create(array(
                "episode_number" => "340",
                "show_date"      => "2011-09-18",
                "title"          => "Tart Cherry Juice!",
                "link"           => "http://www.noagendanation.com/archive/340",
            )
        );
        Episode::create(array(
                "episode_number" => "341",
                "show_date"      => "2011-09-22",
                "title"          => "Selling the Monet",
                "link"           => "http://www.noagendanation.com/archive/341",
            )
        );
        Episode::create(array(
                "episode_number" => "342",
                "show_date"      => "2011-09-25",
                "title"          => "Karma Kards",
                "link"           => "http://www.noagendanation.com/archive/342",
            )
        );
        Episode::create(array(
                "episode_number" => "343",
                "show_date"      => "2011-09-29",
                "title"          => "ZomBin Laden",
                "link"           => "http://www.noagendanation.com/archive/343",
            )
        );
        Episode::create(array(
                "episode_number" => "344",
                "show_date"      => "2011-10-02",
                "title"          => "Cyber Master",
                "link"           => "http://www.noagendanation.com/archive/344",
            )
        );
        Episode::create(array(
                "episode_number" => "345",
                "show_date"      => "2011-10-06",
                "title"          => "Hornbag",
                "link"           => "http://www.noagendanation.com/archive/345",
            )
        );
        Episode::create(array(
                "episode_number" => "346",
                "show_date"      => "2011-10-09",
                "title"          => "The Indignati",
                "link"           => "http://www.noagendanation.com/archive/346",
            )
        );
        Episode::create(array(
                "episode_number" => "347",
                "show_date"      => "2011-10-13",
                "title"          => "Hackerocity",
                "link"           => "http://www.noagendanation.com/archive/347",
            )
        );
        Episode::create(array(
                "episode_number" => "348",
                "show_date"      => "2011-10-16",
                "title"          => "DroneWolf.com",
                "link"           => "http://www.noagendanation.com/archive/348",
            )
        );
        Episode::create(array(
                "episode_number" => "349",
                "show_date"      => "2011-10-20",
                "title"          => "Grandma Clinton",
                "link"           => "http://www.noagendanation.com/archive/349",
            )
        );
        Episode::create(array(
                "episode_number" => "350",
                "show_date"      => "2011-10-23",
                "title"          => "Lady McDeath",
                "link"           => "http://www.noagendanation.com/archive/350",
            )
        );
        Episode::create(array(
                "episode_number" => "351",
                "show_date"      => "2011-10-27",
                "title"          => "Home for the Holidays",
                "link"           => "http://www.noagendanation.com/archive/351",
            )
        );
        Episode::create(array(
                "episode_number" => "352",
                "show_date"      => "2011-10-30",
                "title"          => "Bean Bag Drone",
                "link"           => "http://www.noagendanation.com/archive/352",
            )
        );
        Episode::create(array(
                "episode_number" => "353",
                "show_date"      => "2011-11-03",
                "title"          => "We Can't Wait",
                "link"           => "http://www.noagendanation.com/archive/353",
            )
        );
        Episode::create(array(
                "episode_number" => "354",
                "show_date"      => "2011-11-07",
                "title"          => "Punk Media",
                "link"           => "http://www.noagendanation.com/archive/354",
            )
        );
        Episode::create(array(
                "episode_number" => "355",
                "show_date"      => "2011-11-10",
                "title"          => "Flying Antenna",
                "link"           => "http://www.noagendanation.com/archive/355",
            )
        );
        Episode::create(array(
                "episode_number" => "356",
                "show_date"      => "2011-11-13",
                "title"          => "Super Duper Space Wrench",
                "link"           => "http://www.noagendanation.com/archive/356",
            )
        );
        Episode::create(array(
                "episode_number" => "357",
                "show_date"      => "2011-11-17",
                "title"          => "Rotational Deployment",
                "link"           => "http://www.noagendanation.com/archive/357",
            )
        );
        Episode::create(array(
                "episode_number" => "358",
                "show_date"      => "2011-11-20",
                "title"          => "Bogative Charity",
                "link"           => "http://www.noagendanation.com/archive/358",
            )
        );
        Episode::create(array(
                "episode_number" => "359",
                "show_date"      => "2011-11-24",
                "title"          => "First Buddy",
                "link"           => "http://www.noagendanation.com/archive/359",
            )
        );
        Episode::create(array(
                "episode_number" => "360",
                "show_date"      => "2011-11-27",
                "title"          => "Pencil of Promise",
                "link"           => "http://www.noagendanation.com/archive/360",
            )
        );
        Episode::create(array(
                "episode_number" => "361",
                "show_date"      => "2011-12-01",
                "title"          => "The Fact of the Bladder",
                "link"           => "http://www.noagendanation.com/archive/361",
            )
        );
        Episode::create(array(
                "episode_number" => "362",
                "show_date"      => "2011-12-04",
                "title"          => "Drone Journalism",
                "link"           => "http://www.noagendanation.com/archive/362",
            )
        );
        Episode::create(array(
                "episode_number" => "363",
                "show_date"      => "2011-12-08",
                "title"          => "Mothership Uncloaking?",
                "link"           => "http://www.noagendanation.com/archive/363",
            )
        );
        Episode::create(array(
                "episode_number" => "364",
                "show_date"      => "2011-12-11",
                "title"          => "Katy Bar The Door, Baby!",
                "link"           => "http://www.noagendanation.com/archive/364",
            )
        );
        Episode::create(array(
                "episode_number" => "365",
                "show_date"      => "2011-12-15",
                "title"          => "Trojan Horse",
                "link"           => "http://www.noagendanation.com/archive/365",
            )
        );
        Episode::create(array(
                "episode_number" => "366",
                "show_date"      => "2011-12-18",
                "title"          => "Fools & Knaves",
                "link"           => "http://www.noagendanation.com/archive/366",
            )
        );
        Episode::create(array(
                "episode_number" => "367",
                "show_date"      => "2011-12-22",
                "title"          => "Hats of State",
                "link"           => "http://www.noagendanation.com/archive/367",
            )
        );
        Episode::create(array(
                "episode_number" => "368",
                "show_date"      => "2011-12-25",
                "title"          => "Too Many Clips",
                "link"           => "http://www.noagendanation.com/archive/368",
            )
        );
        Episode::create(array(
                "episode_number" => "369",
                "show_date"      => "2011-12-29",
                "title"          => "Phobos Grunts",
                "link"           => "http://www.noagendanation.com/archive/369",
            )
        );
        Episode::create(array(
                "episode_number" => "370",
                "show_date"      => "2012-01-01",
                "title"          => "Tag and Track",
                "link"           => "http://www.noagendanation.com/archive/370",
            )
        );
        Episode::create(array(
                "episode_number" => "371",
                "show_date"      => "2012-01-05",
                "title"          => "Dead Herring in Norway!",
                "link"           => "http://www.noagendanation.com/archive/371",
            )
        );
        Episode::create(array(
                "episode_number" => "372",
                "show_date"      => "2012-01-08",
                "title"          => "Free Ponies for Everyone!",
                "link"           => "http://www.noagendanation.com/archive/372",
            )
        );
        Episode::create(array(
                "episode_number" => "373",
                "show_date"      => "2012-01-12",
                "title"          => "Paraphilia",
                "link"           => "http://www.noagendanation.com/archive/373",
            )
        );
        Episode::create(array(
                "episode_number" => "374",
                "show_date"      => "2012-01-15",
                "title"          => "Fractals on the Bone",
                "link"           => "http://www.noagendanation.com/archive/374",
            )
        );
        Episode::create(array(
                "episode_number" => "375",
                "show_date"      => "2012-01-19",
                "title"          => "Problematic Woman",
                "link"           => "http://www.noagendanation.com/archive/375",
            )
        );
        Episode::create(array(
                "episode_number" => "376",
                "show_date"      => "2012-01-22",
                "title"          => "Party@Ecropolis",
                "link"           => "http://www.noagendanation.com/archive/376",
            )
        );
        Episode::create(array(
                "episode_number" => "377",
                "show_date"      => "2012-01-26",
                "title"          => "Cleanest Dirty Shirt",
                "link"           => "http://www.noagendanation.com/archive/377",
            )
        );
        Episode::create(array(
                "episode_number" => "378",
                "show_date"      => "2012-01-29",
                "title"          => "Pooper & Blitzer",
                "link"           => "http://www.noagendanation.com/archive/378",
            )
        );
        Episode::create(array(
                "episode_number" => "379",
                "show_date"      => "2012-02-02",
                "title"          => "The Soul Train of Podcasts",
                "link"           => "http://www.noagendanation.com/archive/379",
            )
        );
        Episode::create(array(
                "episode_number" => "380",
                "show_date"      => "2012-02-05",
                "title"          => "Trusted Bedouin Sources",
                "link"           => "http://www.noagendanation.com/archive/380",
            )
        );
        Episode::create(array(
                "episode_number" => "381",
                "show_date"      => "2012-02-09",
                "title"          => "Acquisition Malpractice",
                "link"           => "http://www.noagendanation.com/archive/381",
            )
        );
        Episode::create(array(
                "episode_number" => "382",
                "show_date"      => "2012-02-12",
                "title"          => "Apathy Syndrome",
                "link"           => "http://www.noagendanation.com/archive/382",
            )
        );
        Episode::create(array(
                "episode_number" => "383",
                "show_date"      => "2012-02-16",
                "title"          => "Hot Rods",
                "link"           => "http://www.noagendanation.com/archive/383",
            )
        );
        Episode::create(array(
                "episode_number" => "384",
                "show_date"      => "2012-02-19",
                "title"          => "No Specific Plot",
                "link"           => "http://www.noagendanation.com/archive/384",
            )
        );
        Episode::create(array(
                "episode_number" => "385",
                "show_date"      => "2012-02-23",
                "title"          => "CIA vs DIA",
                "link"           => "http://www.noagendanation.com/archive/385",
            )
        );
        Episode::create(array(
                "episode_number" => "386",
                "show_date"      => "2012-02-26",
                "title"          => "Balochistan Baloney",
                "link"           => "http://www.noagendanation.com/archive/386",
            )
        );
        Episode::create(array(
                "episode_number" => "387",
                "show_date"      => "2012-03-01",
                "title"          => "Bear Bile",
                "link"           => "http://www.noagendanation.com/archive/387",
            )
        );
        Episode::create(array(
                "episode_number" => "388",
                "show_date"      => "2012-03-04",
                "title"          => "Apes on iPads",
                "link"           => "http://www.noagendanation.com/archive/388",
            )
        );
        Episode::create(array(
                "episode_number" => "389",
                "show_date"      => "2012-03-08",
                "title"          => "Camels Everywhere!",
                "link"           => "http://www.noagendanation.com/archive/389",
            )
        );
        Episode::create(array(
                "episode_number" => "390",
                "show_date"      => "2012-03-11",
                "title"          => "Threshold Event",
                "link"           => "http://www.noagendanation.com/archive/390",
            )
        );
        Episode::create(array(
                "episode_number" => "391",
                "show_date"      => "2012-03-15",
                "title"          => "Simulation Investment",
                "link"           => "http://www.noagendanation.com/archive/391",
            )
        );
        Episode::create(array(
                "episode_number" => "392",
                "show_date"      => "2012-03-18",
                "title"          => "Shark Orgy",
                "link"           => "http://www.noagendanation.com/archive/392",
            )
        );
        Episode::create(array(
                "episode_number" => "393",
                "show_date"      => "2012-03-22",
                "title"          => "Throwing Yogurt",
                "link"           => "http://www.noagendanation.com/archive/393",
            )
        );
        Episode::create(array(
                "episode_number" => "394",
                "show_date"      => "2012-03-25",
                "title"          => "No Bagles for You!",
                "link"           => "http://www.noagendanation.com/archive/394",
            )
        );
        Episode::create(array(
                "episode_number" => "395",
                "show_date"      => "2012-03-29",
                "title"          => "Multi Modal Mutt",
                "link"           => "http://www.noagendanation.com/archive/395",
            )
        );
        Episode::create(array(
                "episode_number" => "396",
                "show_date"      => "2012-04-01",
                "title"          => "200 Hundred Million Ninjas",
                "link"           => "http://www.noagendanation.com/archive/396",
            )
        );
        Episode::create(array(
                "episode_number" => "397",
                "show_date"      => "2012-04-05",
                "title"          => "Wiggin' Out",
                "link"           => "http://www.noagendanation.com/archive/397",
            )
        );
        Episode::create(array(
                "episode_number" => "398",
                "show_date"      => "2012-04-08",
                "title"          => "Zombie Gun",
                "link"           => "http://www.noagendanation.com/archive/398",
            )
        );
        Episode::create(array(
                "episode_number" => "399",
                "show_date"      => "2012-04-12",
                "title"          => "Hip-Check China",
                "link"           => "http://www.noagendanation.com/archive/399",
            )
        );
        Episode::create(array(
                "episode_number" => "400",
                "show_date"      => "2012-04-15",
                "title"          => "This is How We Spin",
                "link"           => "http://www.noagendanation.com/archive/400",
            )
        );
        Episode::create(array(
                "episode_number" => "401",
                "show_date"      => "2012-04-19",
                "title"          => "The War on Chicken",
                "link"           => "http://www.noagendanation.com/archive/401",
            )
        );
        Episode::create(array(
                "episode_number" => "402",
                "show_date"      => "2012-04-22",
                "title"          => "Drunk or Not Drunk?",
                "link"           => "http://www.noagendanation.com/archive/402",
            )
        );
        Episode::create(array(
                "episode_number" => "403",
                "show_date"      => "2012-04-26",
                "title"          => "Pharmacy in a Fruit",
                "link"           => "http://www.noagendanation.com/archive/403",
            )
        );
        Episode::create(array(
                "episode_number" => "404",
                "show_date"      => "2012-04-29",
                "title"          => "Spy in a Bag",
                "link"           => "http://www.noagendanation.com/archive/404",
            )
        );
        Episode::create(array(
                "episode_number" => "405",
                "show_date"      => "2012-05-03",
                "title"          => "Piles of Pelicans",
                "link"           => "http://www.noagendanation.com/archive/405",
            )
        );
        Episode::create(array(
                "episode_number" => "406",
                "show_date"      => "2012-05-06",
                "title"          => "Zombie Walk",
                "link"           => "http://www.noagendanation.com/archive/406",
            )
        );
        Episode::create(array(
                "episode_number" => "407",
                "show_date"      => "2012-05-10",
                "title"          => "Exploding Dog",
                "link"           => "http://www.noagendanation.com/archive/407",
            )
        );
        Episode::create(array(
                "episode_number" => "408",
                "show_date"      => "2012-05-13",
                "title"          => "Odious Debt",
                "link"           => "http://www.noagendanation.com/archive/408",
            )
        );
        Episode::create(array(
                "episode_number" => "409",
                "show_date"      => "2012-05-17",
                "title"          => "Head Lag",
                "link"           => "http://www.noagendanation.com/archive/409",
            )
        );
        Episode::create(array(
                "episode_number" => "410",
                "show_date"      => "2012-05-20",
                "title"          => "The Cheeseburger Code",
                "link"           => "http://www.noagendanation.com/archive/410",
            )
        );
        Episode::create(array(
                "episode_number" => "411",
                "show_date"      => "2012-05-24",
                "title"          => "Seal Team 666",
                "link"           => "http://www.noagendanation.com/archive/411",
            )
        );
        Episode::create(array(
                "episode_number" => "412",
                "show_date"      => "2012-05-27",
                "title"          => "Red Square Patch",
                "link"           => "http://www.noagendanation.com/archive/412",
            )
        );
        Episode::create(array(
                "episode_number" => "413",
                "show_date"      => "2012-05-31",
                "title"          => "Kill List",
                "link"           => "http://www.noagendanation.com/archive/413",
            )
        );
        Episode::create(array(
                "episode_number" => "414",
                "show_date"      => "2012-06-03",
                "title"          => "Thingamajig",
                "link"           => "http://www.noagendanation.com/archive/414",
            )
        );
        Episode::create(array(
                "episode_number" => "415",
                "show_date"      => "2012-06-07",
                "title"          => "Om the Dome",
                "link"           => "http://www.noagendanation.com/archive/415",
            )
        );
        Episode::create(array(
                "episode_number" => "416",
                "show_date"      => "2012-06-10",
                "title"          => "Datapalooza",
                "link"           => "http://www.noagendanation.com/archive/416",
            )
        );
        Episode::create(array(
                "episode_number" => "417",
                "show_date"      => "2012-06-14",
                "title"          => "Cyber 9/11",
                "link"           => "http://www.noagendanation.com/archive/417",
            )
        );
        Episode::create(array(
                "episode_number" => "418",
                "show_date"      => "2012-06-17",
                "title"          => "Oryx Burgers!",
                "link"           => "http://www.noagendanation.com/archive/418",
            )
        );
        Episode::create(array(
                "episode_number" => "419",
                "show_date"      => "2012-06-21",
                "title"          => "Degrowth",
                "link"           => "http://www.noagendanation.com/archive/419",
            )
        );
        Episode::create(array(
                "episode_number" => "420",
                "show_date"      => "2012-06-24",
                "title"          => "The Data Hole",
                "link"           => "http://www.noagendanation.com/archive/420",
            )
        );
        Episode::create(array(
                "episode_number" => "421",
                "show_date"      => "2012-06-28",
                "title"          => "Don't Be Nosey",
                "link"           => "http://www.noagendanation.com/archive/421",
            )
        );
        Episode::create(array(
                "episode_number" => "422",
                "show_date"      => "2012-07-01",
                "title"          => "Chaff Hat",
                "link"           => "http://www.noagendanation.com/archive/422",
            )
        );
        Episode::create(array(
                "episode_number" => "423",
                "show_date"      => "2012-07-05",
                "title"          => "Wonderful Marinade",
                "link"           => "http://www.noagendanation.com/archive/423",
            )
        );
        Episode::create(array(
                "episode_number" => "424",
                "show_date"      => "2012-07-08",
                "title"          => "Internet Governance",
                "link"           => "http://www.noagendanation.com/archive/424",
            )
        );
        Episode::create(array(
                "episode_number" => "425",
                "show_date"      => "2012-07-12",
                "title"          => "Understanding No Agenda",
                "link"           => "http://www.noagendanation.com/archive/425",
            )
        );
        Episode::create(array(
                "episode_number" => "426",
                "show_date"      => "2012-07-15",
                "title"          => "The Pipeline Report",
                "link"           => "http://www.noagendanation.com/archive/426",
            )
        );
        Episode::create(array(
                "episode_number" => "427",
                "show_date"      => "2012-07-19",
                "title"          => "Huma-Gate",
                "link"           => "http://www.noagendanation.com/archive/427",
            )
        );
        Episode::create(array(
                "episode_number" => "428",
                "show_date"      => "2012-07-22",
                "title"          => "Muslim Hugger",
                "link"           => "http://www.noagendanation.com/archive/428",
            )
        );
        Episode::create(array(
                "episode_number" => "429",
                "show_date"      => "2012-07-26",
                "title"          => "Gross, Surprising & Scary",
                "link"           => "http://www.noagendanation.com/archive/429",
            )
        );
        Episode::create(array(
                "episode_number" => "430",
                "show_date"      => "2012-07-29",
                "title"          => "Burka Bellyflop",
                "link"           => "http://www.noagendanation.com/archive/430",
            )
        );
        Episode::create(array(
                "episode_number" => "431",
                "show_date"      => "2012-08-02",
                "title"          => "Excited Delerium",
                "link"           => "http://www.noagendanation.com/archive/431",
            )
        );
        Episode::create(array(
                "episode_number" => "432",
                "show_date"      => "2012-08-05",
                "title"          => "Felonious Bears",
                "link"           => "http://www.noagendanation.com/archive/432",
            )
        );
        Episode::create(array(
                "episode_number" => "433",
                "show_date"      => "2012-08-09",
                "title"          => "Hanseatic League",
                "link"           => "http://www.noagendanation.com/archive/433",
            )
        );
        Episode::create(array(
                "episode_number" => "434",
                "show_date"      => "2012-08-12",
                "title"          => "Tripwire",
                "link"           => "http://www.noagendanation.com/archive/434",
            )
        );
        Episode::create(array(
                "episode_number" => "435",
                "show_date"      => "2012-08-16",
                "title"          => "Hillary Doesn't Sweat",
                "link"           => "http://www.noagendanation.com/archive/435",
            )
        );
        Episode::create(array(
                "episode_number" => "436",
                "show_date"      => "2012-08-19",
                "title"          => "Mud on the Truck",
                "link"           => "http://www.noagendanation.com/archive/436",
            )
        );
        Episode::create(array(
                "episode_number" => "437",
                "show_date"      => "2012-08-23",
                "title"          => "Swasselnuff",
                "link"           => "http://www.noagendanation.com/archive/437",
            )
        );
        Episode::create(array(
                "episode_number" => "438",
                "show_date"      => "2012-08-26",
                "title"          => "Social Media Weapons",
                "link"           => "http://www.noagendanation.com/archive/438",
            )
        );
        Episode::create(array(
                "episode_number" => "439",
                "show_date"      => "2012-08-30",
                "title"          => "Struggling Masses",
                "link"           => "http://www.noagendanation.com/archive/439",
            )
        );
        Episode::create(array(
                "episode_number" => "440",
                "show_date"      => "2012-09-02",
                "title"          => "It Can't Happen Here",
                "link"           => "http://www.noagendanation.com/archive/440",
            )
        );
        Episode::create(array(
                "episode_number" => "441",
                "show_date"      => "2012-09-06",
                "title"          => "Poison Wheat",
                "link"           => "http://www.noagendanation.com/archive/441",
            )
        );
        Episode::create(array(
                "episode_number" => "442",
                "show_date"      => "2012-09-09",
                "title"          => "Zombie Webinar",
                "link"           => "http://www.noagendanation.com/archive/442",
            )
        );
        Episode::create(array(
                "episode_number" => "443",
                "show_date"      => "2012-09-13",
                "title"          => "Bad Actors",
                "link"           => "http://www.noagendanation.com/archive/443",
            )
        );
        Episode::create(array(
                "episode_number" => "444",
                "show_date"      => "2012-09-16",
                "title"          => "New Red Line",
                "link"           => "http://www.noagendanation.com/archive/444",
            )
        );
        Episode::create(array(
                "episode_number" => "445",
                "show_date"      => "2012-09-20",
                "title"          => "Insider Attacks",
                "link"           => "http://www.noagendanation.com/archive/445",
            )
        );
        Episode::create(array(
                "episode_number" => "446",
                "show_date"      => "2012-09-23",
                "title"          => "The Convincables",
                "link"           => "http://www.noagendanation.com/archive/446",
            )
        );
        Episode::create(array(
                "episode_number" => "447",
                "show_date"      => "2012-09-27",
                "title"          => "Drone Double Tap",
                "link"           => "http://www.noagendanation.com/archive/447",
            )
        );
        Episode::create(array(
                "episode_number" => "448",
                "show_date"      => "2012-09-30",
                "title"          => "Lucy the Luddite",
                "link"           => "http://www.noagendanation.com/archive/448",
            )
        );
        Episode::create(array(
                "episode_number" => "449",
                "show_date"      => "2012-10-04",
                "title"          => "Deficit Pending",
                "link"           => "http://www.noagendanation.com/archive/449",
            )
        );
        Episode::create(array(
                "episode_number" => "450",
                "show_date"      => "2012-10-07",
                "title"          => "LaGarde's List",
                "link"           => "http://www.noagendanation.com/archive/450",
            )
        );
        Episode::create(array(
                "episode_number" => "451",
                "show_date"      => "2012-10-11",
                "title"          => "Mass of Tax Nuts",
                "link"           => "http://www.noagendanation.com/archive/451",
            )
        );
        Episode::create(array(
                "episode_number" => "452",
                "show_date"      => "2012-10-14",
                "title"          => "PERL Harbor",
                "link"           => "http://www.noagendanation.com/archive/452",
            )
        );
        Episode::create(array(
                "episode_number" => "453",
                "show_date"      => "2012-10-18",
                "title"          => "Haldol Dribbler",
                "link"           => "http://www.noagendanation.com/archive/453",
            )
        );
        Episode::create(array(
                "episode_number" => "454",
                "show_date"      => "2012-10-21",
                "title"          => "Going Purple",
                "link"           => "http://www.noagendanation.com/archive/454",
            )
        );
        Episode::create(array(
                "episode_number" => "455",
                "show_date"      => "2012-10-25",
                "title"          => "Disposition Matrix",
                "link"           => "http://www.noagendanation.com/archive/455",
            )
        );
        Episode::create(array(
                "episode_number" => "456",
                "show_date"      => "2012-10-28",
                "title"          => "Sell-ah-bretties",
                "link"           => "http://www.noagendanation.com/archive/456",
            )
        );
        Episode::create(array(
                "episode_number" => "457",
                "show_date"      => "2012-11-01",
                "title"          => "Giblet in EUROLand",
                "link"           => "http://www.noagendanation.com/archive/457",
            )
        );
        Episode::create(array(
                "episode_number" => "458",
                "show_date"      => "2012-11-04",
                "title"          => "Punch a Puppy!",
                "link"           => "http://www.noagendanation.com/archive/458",
            )
        );
        Episode::create(array(
                "episode_number" => "459",
                "show_date"      => "2012-11-08",
                "title"          => "Exactly Similar",
                "link"           => "http://www.noagendanation.com/archive/459",
            )
        );
        Episode::create(array(
                "episode_number" => "460",
                "show_date"      => "2012-11-11",
                "title"          => "Nein Nein Nein Nein",
                "link"           => "http://www.noagendanation.com/archive/460",
            )
        );
        Episode::create(array(
                "episode_number" => "461",
                "show_date"      => "2012-11-15",
                "title"          => "No-Stray Spray",
                "link"           => "http://www.noagendanation.com/archive/461",
            )
        );
        Episode::create(array(
                "episode_number" => "462",
                "show_date"      => "2012-11-18",
                "title"          => "Flaming Groovies",
                "link"           => "http://www.noagendanation.com/archive/462",
            )
        );
        Episode::create(array(
                "episode_number" => "463",
                "show_date"      => "2012-11-22",
                "title"          => "Dead Hand of Bureaucracy",
                "link"           => "http://www.noagendanation.com/archive/463",
            )
        );
        Episode::create(array(
                "episode_number" => "464",
                "show_date"      => "2012-11-25",
                "title"          => "Iron Key",
                "link"           => "http://www.noagendanation.com/archive/464",
            )
        );
        Episode::create(array(
                "episode_number" => "465",
                "show_date"      => "2012-11-29",
                "title"          => "Skimp Layer",
                "link"           => "http://www.noagendanation.com/archive/465",
            )
        );
        Episode::create(array(
                "episode_number" => "466",
                "show_date"      => "2012-12-02",
                "title"          => "Give Peas a Chance",
                "link"           => "http://www.noagendanation.com/archive/466",
            )
        );
        Episode::create(array(
                "episode_number" => "467",
                "show_date"      => "2012-12-06",
                "title"          => "Red Washcloth",
                "link"           => "http://www.noagendanation.com/archive/467",
            )
        );
        Episode::create(array(
                "episode_number" => "468",
                "show_date"      => "2012-12-09",
                "title"          => "Meat Hands",
                "link"           => "http://www.noagendanation.com/archive/468",
            )
        );
        Episode::create(array(
                "episode_number" => "469",
                "show_date"      => "2012-12-13",
                "title"          => "This That and the Other",
                "link"           => "http://www.noagendanation.com/archive/469",
            )
        );
        Episode::create(array(
                "episode_number" => "470",
                "show_date"      => "2012-12-16",
                "title"          => "Civil Society",
                "link"           => "http://www.noagendanation.com/archive/470",
            )
        );
        Episode::create(array(
                "episode_number" => "471",
                "show_date"      => "2012-12-20",
                "title"          => "Spying is Sharing",
                "link"           => "http://www.noagendanation.com/archive/471",
            )
        );
        Episode::create(array(
                "episode_number" => "472",
                "show_date"      => "2012-12-23",
                "title"          => "Conheads in Mexico",
                "link"           => "http://www.noagendanation.com/archive/472",
            )
        );
        Episode::create(array(
                "episode_number" => "473",
                "show_date"      => "2012-12-27",
                "title"          => "Mac and Cheese",
                "link"           => "http://www.noagendanation.com/archive/473",
            )
        );
        Episode::create(array(
                "episode_number" => "474",
                "show_date"      => "2012-12-30",
                "title"          => "Mongolian Hat",
                "link"           => "http://www.noagendanation.com/archive/474",
            )
        );
        Episode::create(array(
                "episode_number" => "475",
                "show_date"      => "2013-01-03",
                "title"          => "Tsunami Bomb",
                "link"           => "http://www.noagendanation.com/archive/475",
            )
        );
        Episode::create(array(
                "episode_number" => "476",
                "show_date"      => "2013-01-06",
                "title"          => "Middle Class Infanteers",
                "link"           => "http://www.noagendanation.com/archive/476",
            )
        );
        Episode::create(array(
                "episode_number" => "477",
                "show_date"      => "2013-01-10",
                "title"          => "Brolf",
                "link"           => "http://www.noagendanation.com/archive/477",
            )
        );
        Episode::create(array(
                "episode_number" => "478",
                "show_date"      => "2013-01-13",
                "title"          => "Cranks and Firebrands",
                "link"           => "http://www.noagendanation.com/archive/478",
            )
        );
        Episode::create(array(
                "episode_number" => "479",
                "show_date"      => "2013-01-17",
                "title"          => "Belieber",
                "link"           => "http://www.noagendanation.com/archive/479",
            )
        );
        Episode::create(array(
                "episode_number" => "480",
                "show_date"      => "2013-01-20",
                "title"          => "War on Brains",
                "link"           => "http://www.noagendanation.com/archive/480",
            )
        );
        Episode::create(array(
                "episode_number" => "481",
                "show_date"      => "2013-01-24",
                "title"          => "Intelligence Product",
                "link"           => "http://www.noagendanation.com/archive/481",
            )
        );
        Episode::create(array(
                "episode_number" => "482",
                "show_date"      => "2013-01-27",
                "title"          => "Media Harmonization",
                "link"           => "http://www.noagendanation.com/archive/482",
            )
        );
        Episode::create(array(
                "episode_number" => "483",
                "show_date"      => "2013-01-31",
                "title"          => "Culture Creationism",
                "link"           => "http://www.noagendanation.com/archive/483",
            )
        );
        Episode::create(array(
                "episode_number" => "484",
                "show_date"      => "2013-02-03",
                "title"          => "Crazed Guzman",
                "link"           => "http://www.noagendanation.com/archive/484",
            )
        );
        Episode::create(array(
                "episode_number" => "485",
                "show_date"      => "2013-02-07",
                "title"          => "Goys with Guns",
                "link"           => "http://www.noagendanation.com/archive/485",
            )
        );
        Episode::create(array(
                "episode_number" => "486",
                "show_date"      => "2013-02-10",
                "title"          => "Bondpocalypse",
                "link"           => "http://www.noagendanation.com/archive/486",
            )
        );
        Episode::create(array(
                "episode_number" => "487",
                "show_date"      => "2013-02-14",
                "title"          => "Red Bag of Poop",
                "link"           => "http://www.noagendanation.com/archive/487",
            )
        );
        Episode::create(array(
                "episode_number" => "488",
                "show_date"      => "2013-02-17",
                "title"          => "Sponsor Influence",
                "link"           => "http://www.noagendanation.com/archive/488",
            )
        );
        Episode::create(array(
                "episode_number" => "489",
                "show_date"      => "2013-02-21",
                "title"          => "Eat a Baseball",
                "link"           => "http://www.noagendanation.com/archive/489",
            )
        );
        Episode::create(array(
                "episode_number" => "490",
                "show_date"      => "2013-02-24",
                "title"          => "Add Bacon",
                "link"           => "http://www.noagendanation.com/archive/490",
            )
        );
        Episode::create(array(
                "episode_number" => "491",
                "show_date"      => "2013-02-28",
                "title"          => "Pope and Change",
                "link"           => "http://www.noagendanation.com/archive/491",
            )
        );
        Episode::create(array(
                "episode_number" => "492",
                "show_date"      => "2013-03-03",
                "title"          => "Pet Food Stamps",
                "link"           => "http://www.noagendanation.com/archive/492",
            )
        );
        Episode::create(array(
                "episode_number" => "493",
                "show_date"      => "2013-03-07",
                "title"          => "Snowquestration",
                "link"           => "http://www.noagendanation.com/archive/493",
            )
        );
        Episode::create(array(
                "episode_number" => "494",
                "show_date"      => "2013-03-10",
                "title"          => "Aid & Comfort",
                "link"           => "http://www.noagendanation.com/archive/494",
            )
        );
        Episode::create(array(
                "episode_number" => "495",
                "show_date"      => "2013-03-14",
                "title"          => "Big Mac & Cheese",
                "link"           => "http://www.noagendanation.com/archive/495",
            )
        );
        Episode::create(array(
                "episode_number" => "496",
                "show_date"      => "2013-03-17",
                "title"          => "Shoot Look Shoot",
                "link"           => "http://www.noagendanation.com/archive/496",
            )
        );
        Episode::create(array(
                "episode_number" => "497",
                "show_date"      => "2013-03-21",
                "title"          => "Raining Scuds",
                "link"           => "http://www.noagendanation.com/archive/497",
            )
        );
        Episode::create(array(
                "episode_number" => "498",
                "show_date"      => "2013-03-24",
                "title"          => "Obey the Giant Voice System!",
                "link"           => "http://www.noagendanation.com/archive/498",
            )
        );
        Episode::create(array(
                "episode_number" => "499",
                "show_date"      => "2013-03-28",
                "title"          => "Spam Horse",
                "link"           => "http://www.noagendanation.com/archive/499",
            )
        );
        Episode::create(array(
                "episode_number" => "500",
                "show_date"      => "2013-03-31",
                "title"          => "Codeword Austin",
                "link"           => "http://www.noagendanation.com/archive/500",
            )
        );
        Episode::create(array(
                "episode_number" => "501",
                "show_date"      => "2013-04-04",
                "title"          => "Resume Normal Activity!",
                "link"           => "http://www.noagendanation.com/archive/501",
            )
        );
        Episode::create(array(
                "episode_number" => "502",
                "show_date"      => "2013-04-07",
                "title"          => "Nuevo Orden Mundial",
                "link"           => "http://www.noagendanation.com/archive/502",
            )
        );
        Episode::create(array(
                "episode_number" => "503",
                "show_date"      => "2013-04-11",
                "title"          => "Ninjas in Mongolia",
                "link"           => "http://www.noagendanation.com/archive/503",
            )
        );
        Episode::create(array(
                "episode_number" => "504",
                "show_date"      => "2013-04-14",
                "title"          => "Twelve Fourteen",
                "link"           => "http://www.noagendanation.com/archive/504",
            )
        );
        Episode::create(array(
                "episode_number" => "505",
                "show_date"      => "2013-04-18",
                "title"          => "Speculation Analysis",
                "link"           => "http://www.noagendanation.com/archive/505",
            )
        );
        Episode::create(array(
                "episode_number" => "506",
                "show_date"      => "2013-04-21",
                "title"          => "Happy Earth Gay",
                "link"           => "http://www.noagendanation.com/archive/506",
            )
        );
        Episode::create(array(
                "episode_number" => "507",
                "show_date"      => "2013-04-25",
                "title"          => "Airplane Apocalypse",
                "link"           => "http://www.noagendanation.com/archive/507",
            )
        );
        Episode::create(array(
                "episode_number" => "508",
                "show_date"      => "2013-04-28",
                "title"          => "Lonely Crazies",
                "link"           => "http://www.noagendanation.com/archive/508",
            )
        );
        Episode::create(array(
                "episode_number" => "509",
                "show_date"      => "2013-05-02",
                "title"          => "DeDe Dinah",
                "link"           => "http://www.noagendanation.com/archive/509",
            )
        );
        Episode::create(array(
                "episode_number" => "510",
                "show_date"      => "2013-05-05",
                "title"          => "Furtive Movement",
                "link"           => "http://www.noagendanation.com/archive/510",
            )
        );
        Episode::create(array(
                "episode_number" => "511",
                "show_date"      => "2013-05-09",
                "title"          => "Warming Up to Iceland",
                "link"           => "http://www.noagendanation.com/archive/511",
            )
        );
        Episode::create(array(
                "episode_number" => "512",
                "show_date"      => "2013-05-12",
                "title"          => "Club Sub",
                "link"           => "http://www.noagendanation.com/archive/512",
            )
        );
        Episode::create(array(
                "episode_number" => "513",
                "show_date"      => "2013-05-16",
                "title"          => "Pre-Dead",
                "link"           => "http://www.noagendanation.com/archive/513",
            )
        );
        Episode::create(array(
                "episode_number" => "514",
                "show_date"      => "2013-05-19",
                "title"          => "Patriotic Printer",
                "link"           => "http://www.noagendanation.com/archive/514",
            )
        );
        Episode::create(array(
                "episode_number" => "515",
                "show_date"      => "2013-05-23",
                "title"          => "Wantonly Podcasting",
                "link"           => "http://www.noagendanation.com/archive/515",
            )
        );
        Episode::create(array(
                "episode_number" => "516",
                "show_date"      => "2013-05-26",
                "title"          => "9/11 Generation",
                "link"           => "http://www.noagendanation.com/archive/516",
            )
        );
        Episode::create(array(
                "episode_number" => "517",
                "show_date"      => "2013-05-30",
                "title"          => "Chubby Hitler",
                "link"           => "http://www.noagendanation.com/archive/517",
            )
        );
        Episode::create(array(
                "episode_number" => "518",
                "show_date"      => "2013-06-02",
                "title"          => "Hot Scene",
                "link"           => "http://www.noagendanation.com/archive/518",
            )
        );
        Episode::create(array(
                "episode_number" => "519",
                "show_date"      => "2013-06-06",
                "title"          => "Freemium Reporter",
                "link"           => "http://www.noagendanation.com/archive/519",
            )
        );
        Episode::create(array(
                "episode_number" => "520",
                "show_date"      => "2013-06-09",
                "title"          => "Kale Donuts",
                "link"           => "http://www.noagendanation.com/archive/520",
            )
        );
        Episode::create(array(
                "episode_number" => "521",
                "show_date"      => "2013-06-13",
                "title"          => "Techno Boondoggle",
                "link"           => "http://www.noagendanation.com/archive/521",
            )
        );
        Episode::create(array(
                "episode_number" => "522",
                "show_date"      => "2013-06-16",
                "title"          => "Hookers on Sale",
                "link"           => "http://www.noagendanation.com/archive/522",
            )
        );
        Episode::create(array(
                "episode_number" => "523",
                "show_date"      => "2013-06-20",
                "title"          => "by Law and by Rule",
                "link"           => "http://www.noagendanation.com/archive/523",
            )
        );
        Episode::create(array(
                "episode_number" => "524",
                "show_date"      => "2013-06-23",
                "title"          => "Bono Douchebag",
                "link"           => "http://www.noagendanation.com/archive/524",
            )
        );
        Episode::create(array(
                "episode_number" => "525",
                "show_date"      => "2013-06-27",
                "title"          => "SnowJob",
                "link"           => "http://www.noagendanation.com/archive/525",
            )
        );
        Episode::create(array(
                "episode_number" => "526",
                "show_date"      => "2013-06-30",
                "title"          => "#meh!",
                "link"           => "http://www.noagendanation.com/archive/526",
            )
        );
        Episode::create(array(
                "episode_number" => "527",
                "show_date"      => "2013-07-04",
                "title"          => "Odious Selfies",
                "link"           => "http://www.noagendanation.com/archive/527",
            )
        );
        Episode::create(array(
                "episode_number" => "528",
                "show_date"      => "2013-07-07",
                "title"          => "Zero Risk Society",
                "link"           => "http://www.noagendanation.com/archive/528",
            )
        );
        Episode::create(array(
                "episode_number" => "529",
                "show_date"      => "2013-07-11",
                "title"          => "No Coup",
                "link"           => "http://www.noagendanation.com/archive/529",
            )
        );
        Episode::create(array(
                "episode_number" => "530",
                "show_date"      => "2013-07-14",
                "title"          => "Boston Brakes",
                "link"           => "http://www.noagendanation.com/archive/530",
            )
        );
        Episode::create(array(
                "episode_number" => "531",
                "show_date"      => "2013-07-18",
                "title"          => "Quantum Dong",
                "link"           => "http://www.noagendanation.com/archive/531",
            )
        );
        Episode::create(array(
                "episode_number" => "532",
                "show_date"      => "2013-07-21",
                "title"          => "Red Cell",
                "link"           => "http://www.noagendanation.com/archive/532",
            )
        );
        Episode::create(array(
                "episode_number" => "533",
                "show_date"      => "2013-07-25",
                "title"          => "Clip Show II",
                "link"           => "http://www.noagendanation.com/archive/533",
            )
        );
        Episode::create(array(
                "episode_number" => "534",
                "show_date"      => "2013-07-28",
                "title"          => "The Interview Show",
                "link"           => "http://www.noagendanation.com/archive/534",
            )
        );
        Episode::create(array(
                "episode_number" => "535",
                "show_date"      => "2013-08-01",
                "title"          => "Swivel-Chair Speed",
                "link"           => "http://www.noagendanation.com/archive/535",
            )
        );
        Episode::create(array(
                "episode_number" => "536",
                "show_date"      => "2013-08-04",
                "title"          => "Ready for Huma",
                "link"           => "http://www.noagendanation.com/archive/536",
            )
        );
        Episode::create(array(
                "episode_number" => "537",
                "show_date"      => "2013-08-08",
                "title"          => "Thick & Creamy",
                "link"           => "http://www.noagendanation.com/archive/537",
            )
        );
        Episode::create(array(
                "episode_number" => "538",
                "show_date"      => "2013-08-11",
                "title"          => "War on Weed",
                "link"           => "http://www.noagendanation.com/archive/538",
            )
        );
        Episode::create(array(
                "episode_number" => "539",
                "show_date"      => "2013-08-15",
                "title"          => "Assume the Position",
                "link"           => "http://www.noagendanation.com/archive/539",
            )
        );
        Episode::create(array(
                "episode_number" => "540",
                "show_date"      => "2013-08-18",
                "title"          => "Tools of Slaughter",
                "link"           => "http://www.noagendanation.com/archive/540",
            )
        );
        Episode::create(array(
                "episode_number" => "541",
                "show_date"      => "2013-08-22",
                "title"          => "Huge Samoan",
                "link"           => "http://www.noagendanation.com/archive/541",
            )
        );
        Episode::create(array(
                "episode_number" => "542",
                "show_date"      => "2013-08-25",
                "title"          => "Gender Dysphoria",
                "link"           => "http://www.noagendanation.com/archive/542",
            )
        );
        Episode::create(array(
                "episode_number" => "543",
                "show_date"      => "2013-08-29",
                "title"          => "Kosovo Protocol",
                "link"           => "http://www.noagendanation.com/archive/543",
            )
        );
        Episode::create(array(
                "episode_number" => "544",
                "show_date"      => "2013-09-01",
                "title"          => "Arab Winter",
                "link"           => "http://www.noagendanation.com/archive/544",
            )
        );
        Episode::create(array(
                "episode_number" => "545",
                "show_date"      => "2013-09-05",
                "title"          => "Lethal Aid",
                "link"           => "http://www.noagendanation.com/archive/545",
            )
        );
        Episode::create(array(
                "episode_number" => "546",
                "show_date"      => "2013-09-08",
                "title"          => "Munich Moment",
                "link"           => "http://www.noagendanation.com/archive/546",
            )
        );
        Episode::create(array(
                "episode_number" => "547",
                "show_date"      => "2013-09-12",
                "title"          => "\"Special\" Cargo",
                "link"           => "http://www.noagendanation.com/archive/547",
            )
        );
        Episode::create(array(
                "episode_number" => "548",
                "show_date"      => "2013-09-15",
                "title"          => "Mournful Mortician",
                "link"           => "http://www.noagendanation.com/archive/548",
            )
        );
        Episode::create(array(
                "episode_number" => "549",
                "show_date"      => "2013-09-19",
                "title"          => "Associative Propaganda",
                "link"           => "http://www.noagendanation.com/archive/549",
            )
        );
        Episode::create(array(
                "episode_number" => "550",
                "show_date"      => "2013-09-22",
                "title"          => "Cyber Insurance",
                "link"           => "http://www.noagendanation.com/archive/550",
            )
        );
        Episode::create(array(
                "episode_number" => "551",
                "show_date"      => "2013-09-26",
                "title"          => "The Tolerance Podcast",
                "link"           => "http://www.noagendanation.com/archive/551",
            )
        );
        Episode::create(array(
                "episode_number" => "552",
                "show_date"      => "2013-09-29",
                "title"          => "Almost Certain = Fact!",
                "link"           => "http://www.noagendanation.com/archive/552",
            )
        );
        Episode::create(array(
                "episode_number" => "553",
                "show_date"      => "2013-10-03",
                "title"          => "Hate-Spewing Hashtags",
                "link"           => "http://www.noagendanation.com/archive/553",
            )
        );
        Episode::create(array(
                "episode_number" => "554",
                "show_date"      => "2013-10-06",
                "title"          => "Slave Bracelet",
                "link"           => "http://www.noagendanation.com/archive/554",
            )
        );
        Episode::create(array(
                "episode_number" => "555",
                "show_date"      => "2013-10-10",
                "title"          => "Grays+Monkeys=Humans",
                "link"           => "http://www.noagendanation.com/archive/555",
            )
        );
        Episode::create(array(
                "episode_number" => "556",
                "show_date"      => "2013-10-13",
                "title"          => "Vape like a Ninja",
                "link"           => "http://www.noagendanation.com/archive/556",
            )
        );
        Episode::create(array(
                "episode_number" => "557",
                "show_date"      => "2013-10-17",
                "title"          => "Kalemia",
                "link"           => "http://www.noagendanation.com/archive/557",
            )
        );
        Episode::create(array(
                "episode_number" => "558",
                "show_date"      => "2013-10-20",
                "title"          => "Clouds of Crisis",
                "link"           => "http://www.noagendanation.com/archive/558",
            )
        );
        Episode::create(array(
                "episode_number" => "559",
                "show_date"      => "2013-10-24",
                "title"          => "Tech Surge",
                "link"           => "http://www.noagendanation.com/archive/559",
            )
        );
        Episode::create(array(
                "episode_number" => "560",
                "show_date"      => "2013-10-27",
                "title"          => "Hornet's Nest",
                "link"           => "http://www.noagendanation.com/archive/560",
            )
        );
        Episode::create(array(
                "episode_number" => "561",
                "show_date"      => "2013-10-31",
                "title"          => "Neuroelasticity",
                "link"           => "http://www.noagendanation.com/archive/561",
            )
        );
        Episode::create(array(
                "episode_number" => "562",
                "show_date"      => "2013-11-03",
                "title"          => "Blotto",
                "link"           => "http://www.noagendanation.com/archive/562",
            )
        );
        Episode::create(array(
                "episode_number" => "563",
                "show_date"      => "2013-11-07",
                "title"          => "Law of the Jungle",
                "link"           => "http://www.noagendanation.com/archive/563",
            )
        );
        Episode::create(array(
                "episode_number" => "564",
                "show_date"      => "2013-11-10",
                "title"          => "Summer of Snowden",
                "link"           => "http://www.noagendanation.com/archive/564",
            )
        );
        Episode::create(array(
                "episode_number" => "565",
                "show_date"      => "2013-11-14",
                "title"          => "Spy in a Bag",
                "link"           => "http://www.noagendanation.com/archive/565",
            )
        );
        Episode::create(array(
                "episode_number" => "566",
                "show_date"      => "2013-11-17",
                "title"          => "Bots & Girls!",
                "link"           => "http://www.noagendanation.com/archive/566",
            )
        );
        Episode::create(array(
                "episode_number" => "567",
                "show_date"      => "2013-11-21",
                "title"          => "Marketecture",
                "link"           => "http://www.noagendanation.com/archive/567",
            )
        );
        Episode::create(array(
                "episode_number" => "568",
                "show_date"      => "2013-11-24",
                "title"          => "Heteroflexible Previvor",
                "link"           => "http://www.noagendanation.com/archive/568",
            )
        );
        Episode::create(array(
                "episode_number" => "569",
                "show_date"      => "2013-11-28",
                "title"          => "23 and Plea",
                "link"           => "http://www.noagendanation.com/archive/569",
            )
        );
        Episode::create(array(
                "episode_number" => "570",
                "show_date"      => "2013-12-01",
                "title"          => "Festival of Corruption",
                "link"           => "http://www.noagendanation.com/archive/570",
            )
        );
        Episode::create(array(
                "episode_number" => "571",
                "show_date"      => "2013-12-05",
                "title"          => "New World Odor",
                "link"           => "http://www.noagendanation.com/archive/571",
            )
        );
        Episode::create(array(
                "episode_number" => "572",
                "show_date"      => "2013-12-08",
                "title"          => "Uptalking Dudes",
                "link"           => "http://www.noagendanation.com/archive/572",
            )
        );
        Episode::create(array(
                "episode_number" => "573",
                "show_date"      => "2013-12-12",
                "title"          => "NEETS",
                "link"           => "http://www.noagendanation.com/archive/573",
            )
        );
        Episode::create(array(
                "episode_number" => "574",
                "show_date"      => "2013-12-15",
                "title"          => "Make Happy",
                "link"           => "http://www.noagendanation.com/archive/574",
            )
        );
        Episode::create(array(
                "episode_number" => "575",
                "show_date"      => "2013-12-19",
                "title"          => "BIOS Brick",
                "link"           => "http://www.noagendanation.com/archive/575",
            )
        );
        Episode::create(array(
                "episode_number" => "576",
                "show_date"      => "2013-12-22",
                "title"          => "Mysterious Erratic",
                "link"           => "http://www.noagendanation.com/archive/576",
            )
        );
        Episode::create(array(
                "episode_number" => "577",
                "show_date"      => "2013-12-26",
                "title"          => "Scripted Fat Talk",
                "link"           => "http://www.noagendanation.com/archive/577",
            )
        );
        Episode::create(array(
                "episode_number" => "578",
                "show_date"      => "2013-12-29",
                "title"          => "Blast Wave Accelerator",
                "link"           => "http://www.noagendanation.com/archive/578",
            )
        );
        Episode::create(array(
                "episode_number" => "579",
                "show_date"      => "2014-01-02",
                "title"          => "Warren, Melinda & I",
                "link"           => "http://www.noagendanation.com/archive/579",
            )
        );
        Episode::create(array(
                "episode_number" => "580",
                "show_date"      => "2014-01-05",
                "title"          => "Hiroshima Syndrome",
                "link"           => "http://www.noagendanation.com/archive/580",
            )
        );
        Episode::create(array(
                "episode_number" => "581",
                "show_date"      => "2014-01-09",
                "title"          => "Message from the Future",
                "link"           => "http://www.noagendanation.com/archive/581",
            )
        );
        Episode::create(array(
                "episode_number" => "582",
                "show_date"      => "2014-01-12",
                "title"          => "Eradicate Misery",
                "link"           => "http://www.noagendanation.com/archive/582",
            )
        );
        Episode::create(array(
                "episode_number" => "583",
                "show_date"      => "2014-01-16",
                "title"          => "A Pure Heart",
                "link"           => "http://www.noagendanation.com/archive/583",
            )
        );
        Episode::create(array(
                "episode_number" => "584",
                "show_date"      => "2014-01-19",
                "title"          => "Hybrid Pigs",
                "link"           => "http://www.noagendanation.com/archive/584",
            )
        );
        Episode::create(array(
                "episode_number" => "585",
                "show_date"      => "2014-01-23",
                "title"          => "Dhimmi or Dead",
                "link"           => "http://www.noagendanation.com/archive/585",
            )
        );
        Episode::create(array(
                "episode_number" => "586",
                "show_date"      => "2014-01-26",
                "title"          => "Sustainabale Orban",
                "link"           => "http://www.noagendanation.com/archive/586",
            )
        );
        Episode::create(array(
                "episode_number" => "587",
                "show_date"      => "2014-01-30",
                "title"          => "People the Board",
                "link"           => "http://www.noagendanation.com/archive/587",
            )
        );
        Episode::create(array(
                "episode_number" => "588",
                "show_date"      => "2014-02-02",
                "title"          => "Velveeta Shortage!",
                "link"           => "http://www.noagendanation.com/archive/588",
            )
        );
        Episode::create(array(
                "episode_number" => "589",
                "show_date"      => "2014-02-06",
                "title"          => "Guards Gates Guns",
                "link"           => "http://www.noagendanation.com/archive/589",
            )
        );
        Episode::create(array(
                "episode_number" => "590",
                "show_date"      => "2014-02-09",
                "title"          => "Jelly Side Up",
                "link"           => "http://www.noagendanation.com/archive/590",
            )
        );
        Episode::create(array(
                "episode_number" => "591",
                "show_date"      => "2014-02-13",
                "title"          => "Mipster Intercept",
                "link"           => "http://www.noagendanation.com/archive/591",
            )
        );
        Episode::create(array(
                "episode_number" => "592",
                "show_date"      => "2014-02-16",
                "title"          => "Taser Taser Taser!",
                "link"           => "http://www.noagendanation.com/archive/592",
            )
        );
        Episode::create(array(
                "episode_number" => "593",
                "show_date"      => "2014-02-20",
                "title"          => "Abundance of Caution",
                "link"           => "http://www.noagendanation.com/archive/593",
            )
        );
        Episode::create(array(
                "episode_number" => "594",
                "show_date"      => "2014-02-23",
                "title"          => "Tears Will Come",
                "link"           => "http://www.noagendanation.com/archive/594",
            )
        );
        Episode::create(array(
                "episode_number" => "595",
                "show_date"      => "2014-02-27",
                "title"          => "Ottomania!",
                "link"           => "http://www.noagendanation.com/archive/595",
            )
        );
        Episode::create(array(
                "episode_number" => "596",
                "show_date"      => "2014-03-02",
                "title"          => "Undesirable Nudity",
                "link"           => "http://www.noagendanation.com/archive/596",
            )
        );
        Episode::create(array(
                "episode_number" => "597",
                "show_date"      => "2014-03-06",
                "title"          => "Prison Prep",
                "link"           => "http://www.noagendanation.com/archive/597",
            )
        );
        Episode::create(array(
                "episode_number" => "598",
                "show_date"      => "2014-03-09",
                "title"          => "Experiential Evidence",
                "link"           => "http://www.noagendanation.com/archive/598",
            )
        );
        Episode::create(array(
                "episode_number" => "599",
                "show_date"      => "2014-03-13",
                "title"          => "Nuclear Tipped",
                "link"           => "http://www.noagendanation.com/archive/599",
            )
        );
        Episode::create(array(
                "episode_number" => "600",
                "show_date"      => "2014-03-16",
                "title"          => "Seven Proxies",
                "link"           => "http://www.noagendanation.com/archive/600",
            )
        );
        Episode::create(array(
                "episode_number" => "601",
                "show_date"      => "2014-03-20",
                "title"          => "Pilots of Terror",
                "link"           => "http://www.noagendanation.com/archive/601",
            )
        );
        Episode::create(array(
                "episode_number" => "602",
                "show_date"      => "2014-03-23",
                "title"          => "twitter mwitter",
                "link"           => "http://www.noagendanation.com/archive/602",
            )
        );
        Episode::create(array(
                "episode_number" => "603",
                "show_date"      => "2014-03-27",
                "title"          => "Revolution of Dignity",
                "link"           => "http://www.noagendanation.com/archive/603",
            )
        );
        Episode::create(array(
                "episode_number" => "604",
                "show_date"      => "2014-03-30",
                "title"          => "Dead Jellyfish",
                "link"           => "http://www.noagendanation.com/archive/604",
            )
        );
        Episode::create(array(
                "episode_number" => "605",
                "show_date"      => "2014-04-03",
                "title"          => "Biostitutes",
                "link"           => "http://www.noagendanation.com/archive/605",
            )
        );
        Episode::create(array(
                "episode_number" => "606",
                "show_date"      => "2014-04-06",
                "title"          => "Get Ready to Rubble",
                "link"           => "http://www.noagendanation.com/archive/606",
            )
        );
        Episode::create(array(
                "episode_number" => "607",
                "show_date"      => "2014-04-10",
                "title"          => "Big Sandy",
                "link"           => "http://www.noagendanation.com/archive/607",
            )
        );
        Episode::create(array(
                "episode_number" => "608",
                "show_date"      => "2014-04-13",
                "title"          => "Cli-Fi®",
                "link"           => "http://www.noagendanation.com/archive/608",
            )
        );
        Episode::create(array(
                "episode_number" => "609",
                "show_date"      => "2014-04-17",
                "title"          => "Johnson's Johnson, Jump!",
                "link"           => "http://www.noagendanation.com/archive/609",
            )
        );
        Episode::create(array(
                "episode_number" => "610",
                "show_date"      => "2014-04-17",
                "title"          => "Clip Show III!",
                "link"           => "http://www.noagendanation.com/archive/610",
            )
        );
        Episode::create(array(
                "episode_number" => "611",
                "show_date"      => "2014-04-24",
                "title"          => "Let's Get Social!",
                "link"           => "http://www.noagendanation.com/archive/611",
            )
        );
        Episode::create(array(
                "episode_number" => "612",
                "show_date"      => "2014-04-27",
                "title"          => "Cradle to Career!",
                "link"           => "http://www.noagendanation.com/archive/612",
            )
        );
        Episode::create(array(
                "episode_number" => "613",
                "show_date"      => "2014-05-01",
                "title"          => "Carbon Overload!",
                "link"           => "http://www.noagendanation.com/archive/613",
            )
        );
        Episode::create(array(
                "episode_number" => "614",
                "show_date"      => "2014-05-04",
                "title"          => "Fruity Drinks!",
                "link"           => "http://www.noagendanation.com/archive/614",
            )
        );
        Episode::create(array(
                "episode_number" => "615",
                "show_date"      => "2014-05-08",
                "title"          => "Gravity Bomb!",
                "link"           => "http://www.noagendanation.com/archive/615",
            )
        );
        Episode::create(array(
                "episode_number" => "616",
                "show_date"      => "2014-05-11",
                "title"          => "Jihadi Disneyland",
                "link"           => "http://www.noagendanation.com/archive/616",
            )
        );
        Episode::create(array(
                "episode_number" => "617",
                "show_date"      => "2014-05-15",
                "title"          => "Climate Chaos",
                "link"           => "http://www.noagendanation.com/archive/617",
            )
        );
        Episode::create(array(
                "episode_number" => "618",
                "show_date"      => "2014-05-18",
                "title"          => "Binge Watch",
                "link"           => "http://www.noagendanation.com/archive/618",
            )
        );
        Episode::create(array(
                "episode_number" => "619",
                "show_date"      => "2014-05-22",
                "title"          => "Droves of Jihadis",
                "link"           => "http://www.noagendanation.com/archive/619",
            )
        );
        Episode::create(array(
                "episode_number" => "620",
                "show_date"      => "2014-05-25",
                "title"          => "Dead Man Cuffed",
                "link"           => "http://www.noagendanation.com/archive/620",
            )
        );
        Episode::create(array(
                "episode_number" => "621",
                "show_date"      => "2014-05-29",
                "title"          => "Gun Extremists",
                "link"           => "http://www.noagendanation.com/archive/621",
            )
        );
        Episode::create(array(
                "episode_number" => "622",
                "show_date"      => "2014-06-01",
                "title"          => "Operation Chokepoint",
                "link"           => "http://www.noagendanation.com/archive/622",
            )
        );
        Episode::create(array(
                "episode_number" => "623",
                "show_date"      => "2014-06-05",
                "title"          => "\"Fact Pattern\"",
                "link"           => "http://www.noagendanation.com/archive/623",
            )
        );
        Episode::create(array(
                "episode_number" => "624",
                "show_date"      => "2014-06-08",
                "title"          => "The Sluggish Cloud",
                "link"           => "http://www.noagendanation.com/archive/624",
            )
        );
        Episode::create(array(
                "episode_number" => "625",
                "show_date"      => "2014-06-12",
                "title"          => "Touching the Stick",
                "link"           => "http://www.noagendanation.com/archive/625",
            )
        );
        Episode::create(array(
                "episode_number" => "626",
                "show_date"      => "2014-06-15",
                "title"          => "Preemptive Prosecution",
                "link"           => "http://www.noagendanation.com/archive/626",
            )
        );
        Episode::create(array(
                "episode_number" => "627",
                "show_date"      => "2014-06-19",
                "title"          => "Scam Celebrities",
                "link"           => "http://www.noagendanation.com/archive/627",
            )
        );
        Episode::create(array(
                "episode_number" => "628",
                "show_date"      => "2014-06-22",
                "title"          => "Italian Lightning",
                "link"           => "http://www.noagendanation.com/archive/628",
            )
        );
        Episode::create(array(
                "episode_number" => "629",
                "show_date"      => "2014-06-26",
                "title"          => "Passport Terrorists!",
                "link"           => "http://www.noagendanation.com/archive/629",
            )
        );
        Episode::create(array(
                "episode_number" => "630",
                "show_date"      => "2014-06-29",
                "title"          => "Double Twister",
                "link"           => "http://www.noagendanation.com/archive/630",
            )
        );
        Episode::create(array(
                "episode_number" => "631",
                "show_date"      => "2014-07-03",
                "title"          => "Micro Propaganda",
                "link"           => "http://www.noagendanation.com/archive/631",
            )
        );
        Episode::create(array(
                "episode_number" => "632",
                "show_date"      => "2014-07-06",
                "title"          => "The Weed Mobile",
                "link"           => "http://www.noagendanation.com/archive/632",
            )
        );
        Episode::create(array(
                "episode_number" => "633",
                "show_date"      => "2014-07-10",
                "title"          => "Reverse the Curse",
                "link"           => "http://www.noagendanation.com/archive/633",
            )
        );
        Episode::create(array(
                "episode_number" => "634",
                "show_date"      => "2014-07-13",
                "title"          => "Rough Patch",
                "link"           => "http://www.noagendanation.com/archive/634",
            )
        );
        Episode::create(array(
                "episode_number" => "635",
                "show_date"      => "2014-07-17",
                "title"          => "28 Pages",
                "link"           => "http://www.noagendanation.com/archive/635",
            )
        );
        Episode::create(array(
                "episode_number" => "636",
                "show_date"      => "2014-07-20",
                "title"          => "Appification Generation",
                "link"           => "http://www.noagendanation.com/archive/636",
            )
        );
        Episode::create(array(
                "episode_number" => "637",
                "show_date"      => "2014-07-24",
                "title"          => "Common Sense Fact",
                "link"           => "http://www.noagendanation.com/archive/637",
            )
        );
        Episode::create(array(
                "episode_number" => "638",
                "show_date"      => "2014-07-27",
                "title"          => "Parliment Update",
                "link"           => "http://www.noagendanation.com/archive/638",
            )
        );
        Episode::create(array(
                "episode_number" => "639",
                "show_date"      => "2014-07-31",
                "title"          => "Tangible Things",
                "link"           => "http://www.noagendanation.com/archive/639",
            )
        );
        Episode::create(array(
                "episode_number" => "640",
                "show_date"      => "2014-08-03",
                "title"          => "Putinism",
                "link"           => "http://www.noagendanation.com/archive/640",
            )
        );
        Episode::create(array(
                "episode_number" => "641",
                "show_date"      => "2014-08-07",
                "title"          => "Extractive",
                "link"           => "http://www.noagendanation.com/archive/641",
            )
        );
        Episode::create(array(
                "episode_number" => "642",
                "show_date"      => "2014-08-10",
                "title"          => "Walking Bear",
                "link"           => "http://www.noagendanation.com/archive/642",
            )
        );
        Episode::create(array(
                "episode_number" => "643",
                "show_date"      => "2014-08-14",
                "title"          => "Brand Snowden",
                "link"           => "http://www.noagendanation.com/archive/643",
            )
        );
        Episode::create(array(
                "episode_number" => "644",
                "show_date"      => "2014-08-17",
                "title"          => "Why Why Not",
                "link"           => "http://www.noagendanation.com/archive/644",
            )
        );
        Episode::create(array(
                "episode_number" => "645",
                "show_date"      => "2014-08-21",
                "title"          => "Ghost of Austin",
                "link"           => "http://www.noagendanation.com/archive/645",
            )
        );
        Episode::create(array(
                "episode_number" => "646",
                "show_date"      => "2014-08-24",
                "title"          => "Boundless Barbarity",
                "link"           => "http://www.noagendanation.com/archive/646",
            )
        );
        Episode::create(array(
                "episode_number" => "647",
                "show_date"      => "2014-08-28",
                "title"          => "Flood the Zone",
                "link"           => "http://www.noagendanation.com/archive/647",
            )
        );
        Episode::create(array(
                "episode_number" => "648",
                "show_date"      => "2014-08-31",
                "title"          => "Centrifuge Him!",
                "link"           => "http://www.noagendanation.com/archive/648",
            )
        );
        Episode::create(array(
                "episode_number" => "649",
                "show_date"      => "2014-09-04",
                "title"          => "Scottish Do Over",
                "link"           => "http://www.noagendanation.com/archive/649",
            )
        );
        Episode::create(array(
                "episode_number" => "650",
                "show_date"      => "2014-09-07",
                "title"          => "Summer of Blood",
                "link"           => "http://www.noagendanation.com/archive/650",
            )
        );
        Episode::create(array(
                "episode_number" => "651",
                "show_date"      => "2014-09-11",
                "title"          => "Plague Grenade",
                "link"           => "http://www.noagendanation.com/archive/651",
            )
        );
        Episode::create(array(
                "episode_number" => "652",
                "show_date"      => "2014-09-14",
                "title"          => "After Spin Class",
                "link"           => "http://www.noagendanation.com/archive/652",
            )
        );
        Episode::create(array(
                "episode_number" => "653",
                "show_date"      => "2014-09-18",
                "title"          => "Evil Layer Cake",
                "link"           => "http://www.noagendanation.com/archive/653",
            )
        );
        Episode::create(array(
                "episode_number" => "654",
                "show_date"      => "2014-09-21",
                "title"          => "Q-Burn",
                "link"           => "http://www.noagendanation.com/archive/654",
            )
        );
        Episode::create(array(
                "episode_number" => "655",
                "show_date"      => "2014-09-25",
                "title"          => "Network of Death",
                "link"           => "http://www.noagendanation.com/archive/655",
            )
        );
        Episode::create(array(
                "episode_number" => "656",
                "show_date"      => "2014-09-28",
                "title"          => "Gap Focused Thinking",
                "link"           => "http://www.noagendanation.com/archive/656",
            )
        );
        Episode::create(array(
                "episode_number" => "657",
                "show_date"      => "2014-10-02",
                "title"          => "bio-hacking",
                "link"           => "http://www.noagendanation.com/archive/657",
            )
        );
        Episode::create(array(
                "episode_number" => "658",
                "show_date"      => "2014-10-05",
                "title"          => "It's a Glitch!",
                "link"           => "http://www.noagendanation.com/archive/658",
            )
        );
        Episode::create(array(
                "episode_number" => "659",
                "show_date"      => "2014-10-09",
                "title"          => "Mirific!",
                "link"           => "http://www.noagendanation.com/archive/659",
            )
        );
        Episode::create(array(
                "episode_number" => "660",
                "show_date"      => "2014-10-12",
                "title"          => "Clog the Pipes",
                "link"           => "http://www.noagendanation.com/archive/660",
            )
        );
        Episode::create(array(
                "episode_number" => "661",
                "show_date"      => "2014-10-16",
                "title"          => "Speciesism",
                "link"           => "http://www.noagendanation.com/archive/661",
            )
        );
        Episode::create(array(
                "episode_number" => "662",
                "show_date"      => "2014-10-19",
                "title"          => "Barama",
                "link"           => "http://www.noagendanation.com/archive/662",
            )
        );
        Episode::create(array(
                "episode_number" => "663",
                "show_date"      => "2014-10-23",
                "title"          => "Pupil Progression Plan",
                "link"           => "http://www.noagendanation.com/archive/663",
            )
        );
        Episode::create(array(
                "episode_number" => "664",
                "show_date"      => "2014-10-26",
                "title"          => "Boss Ass Look",
                "link"           => "http://www.noagendanation.com/archive/664",
            )
        );
        Episode::create(array(
                "episode_number" => "665",
                "show_date"      => "2014-10-30",
                "title"          => "TBD",
                "link"           => '',
            )
        );
        Episode::create(array(
                "episode_number" => "666",
                "show_date"      => "2014-11-02",
                "title"          => "The Beast Episode",
                "link"           => '',
            )
        );

    }

}
