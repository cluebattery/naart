<?php

class RolesTableSeeder extends Seeder {

    public function run()
    {
        //DB::table('roles')->delete();

        $adminRole = Role::where('id', '=', 1)->first();

        $user = User::where('username','=','Bonked')->first();
        $user->attachRole( $adminRole );

        $user = User::where('username','=','Crackpot')->first();
        $user->attachRole( $adminRole );

        $user = User::where('username','=','Buzzkill')->first();
        $user->attachRole( $adminRole );

        $user = User::where('username','=','NICKtheRAT')->first();
        $user->attachRole( $adminRole );
    }

}
