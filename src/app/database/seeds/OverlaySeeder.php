<?php

class OverlaySeeder extends Seeder
{

    public function run()
    {
        $this->call('OverlayTableSeeder');

        $this->command->info('Overlay table seeded!');
    }

}

class OverlayTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('overlays')->delete();

        Overlay::create(array(
            'user_id'  => 1,
            'title'    => 'Sir Randy Asher Style',
            'filename' => 'sirrandyasheroverlay.png',
            'description' => 'The original standard for No Agenda Album Art',
            'approved' => true)
        );
        Overlay::create(array(
                'user_id'  => 1,
                'title'    => 'Sir Paul T. Style',
                'filename' => 'sirpaultoverlay.png',
                'description' => 'An updated overlay with tons of use since 2010 for No Agenda Album Art',
                'approved' => true)
        );


    }

}
