@extends('layouts.master')

@section('title')
@parent
 :: {{ $title }}
@stop

@section('content')
    <div class="container">
	    <div class="row center">
            <h1>Drag and Drop Your Artwork or Click Below to Start</h1>
            <p>Once you have uploaded the artwork, you will be able to crop it and choose the overlay you want to apply (if any). You can choose to suggest that the artwork be used for the next episode or added to the Evergreen collection. It is a pretty straight-forward process, remember to give your artwork a title and you can also change the display name for yourself on your <a href="/artist/{{ Auth::user()->id }}">Artist Profile Page</a>.</p>
        </div>
        <div class="row">
            @if (Session::get('error'))
                <div class="alert alert-error alert-danger">
                    @if (is_array(Session::get('error')))
                        {{ head(Session::get('error')) }}
                    @endif
                    <p><strong>NOTE:</strong> If you receive the error "The newimagepath field is required" - and you actually provided an image - then more than likely there is a problem with that particular image that is preventing it from being processed by the server. Almost every single time this is because the files is too large or in a format like bitmap that shouldn't be being used on the Internet because it isn't the 1980s anymore, etc.  Try saving the image as a png or jpg.</p>
                    <p>If that still doesn't fix the issue - instead of harrassing Adam and John via email/twitter/smoke signal (since they don't run this site or write the code that makes it work,) <strong><em><a href="https://bitbucket.org/cluebattery/naart/issues">Open an issue letting the developers know</a></em></strong> so that something can actually be done to fix the problem. Be sure to include the exact image file giving you trouble in the attachments of said issue so we don't have to play a guessing game about what might have been problematic for the server to process.</p> 
                </div>
            @endif

            @if (Session::get('notice'))
                <div class="alert">{{ Session::get('notice') }}</div>
            @endif
            <div id="dropbox" class="center">
                <input type="hidden" id="_token" name="_token" value="{{{ csrf_token() }}}" />
                <h1 id="upinstruct"><span class="fa fa-cloud-upload fa-5x"></span><br>Drop your image here to upload</h1>
                <input type="file" id="artworkfile" style="display:none">
            </div>
            <div class="preview"></div>
            <div class="img-container">
        </div>
            <div class="img-preview"></div>
            <form id="gostep2" role="form" method="post" action="/crop">
                <input type="hidden" id="newimagepath" name="newimagepath" value="">
                <input type="hidden" id="newimagename" name="newimagename" value="">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
            <form>
        </div>
	</div>
@stop

@section('scripts')
@parent
<script src="/assets/js/docreate.js"></script>
@stop
