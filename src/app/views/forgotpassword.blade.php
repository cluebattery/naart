@extends('layouts.master')

@section('title')@parent:: Forgot Password @stop

@section('content')
    <div class="container">
        <h1>Forgot Your Password?</h1>
        <p>Enter your email in the form below and we'll send you a password reset link to get back in.</p>
        <form method="POST" action="{{ URL::to('/users/forgot_password') }}" accept-charset="UTF-8">
            <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
            <div class="form-group">
                <label for="email">{{{ Lang::get('confide::confide.e_mail') }}}</label>
                <div class="input-append input-group">
                    <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
                    <span class="input-group-btn">
                        <input class="btn btn-default" type="submit" value="{{{ Lang::get('confide::confide.forgot.submit') }}}">
                    </span>
                </div>
            </div>
        </form>
    </div>
@stop
