<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Carbon\Carbon;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements ConfideUserInterface
{
    use ConfideUser;
    use HasRole;

    public static function boot()
    {
        parent::boot();

        static::created(function($user)
        {
            $profile = new Profile;
            $profile->user_id = $user->id;
            $profile->name = $user->username;
            $profile->save();
        });
    }

    public function profile()
    {
        return $this->hasOne('Profile');
    }

    public function artworks()
    {
        return $this->hasMany('Artwork');
    }

    public function overlays()
    {
        return $this->hasMany('Overlay');
    }

    public function episodes()
    {
        return $this->hasManyThrough('Episode', 'Artwork');
    }

    public function getUsersByPartialname($partialname)
    {
        return $this->where('username', 'like', '%' . $partialname . '%')->orderBy(DB::raw('LOCATE ("' . $partialname . '", username)'));
    }

    /**
     * Get user by username
     * @param $username
     * @return mixed
     */
    public function getUserByUsername( $username )
    {
        return $this->where('username', '=', $username)->first();
    }

    /**
     * Get the date the user was created.
     *
     * @return string
     */
    public function joined()
    {
        return String::date(Carbon::createFromFormat('Y-n-j G:i:s', $this->created_at));
    }

    /**
     * Save roles inputted from multiselect
     * @param $inputRoles
     */
    public function saveRoles($inputRoles)
    {
        if(! empty($inputRoles)) {
            $this->roles()->sync($inputRoles);
        } else {
            $this->roles()->detach();
        }
    }

    /**
     * Returns user's current role ids only.
     * @return array|bool
     */
    public function currentRoleIds()
    {
        $roles = $this->roles;
        $roleIds = false;
        if( !empty( $roles ) ) {
            $roleIds = array();
            foreach( $roles as &$role )
            {
                $roleIds[] = $role->id;
            }
        }
        return $roleIds;
    }
}
